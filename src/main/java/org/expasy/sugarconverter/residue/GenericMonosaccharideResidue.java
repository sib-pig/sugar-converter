package org.expasy.sugarconverter.residue;

import java.util.ArrayList;
import org.expasy.sugarconverter.parser.Constants;

import org.expasy.sugarconverter.sugar.*;


public class GenericMonosaccharideResidue extends AbstractResidue{

	
	private Anomer anomer = Anomer.UnknownAnomer;
		
	private Monosaccharide monosaccharide;
	private int CtRESnumber;
	
	private String RingClosureStart = Constants.UNKNOWN.toUpperCase();
	private String RingClosureEnd = Constants.UNKNOWN.toUpperCase();	
	
	private ArrayList<MonosaccharideModifier> modifiers = new ArrayList<MonosaccharideModifier>();

	
	/*Constructor */	
	public GenericMonosaccharideResidue()
	{
		
	}
	
	/*Accessor */
	public Monosaccharide getMonosaccharide() {
		return monosaccharide;
	}

	public void setMonosaccharide(Monosaccharide monosaccharide) {
		setRingClosureDefault(monosaccharide.getSuperClass());
		this.monosaccharide = monosaccharide;
	}

	public int getCtRESnumber() {
		return CtRESnumber;
	}

	public void setCtRESnumber(int CtRESnumber) {
		this.CtRESnumber = CtRESnumber;
	}
	

	public Anomer getAnomer() {
		return anomer;
	}
	
	public void setAnomer(Anomer anomer) {
		this.anomer = anomer;
	}
	
	public void setAnomer(String anomer) {
		this.anomer = Anomer.fromString(anomer);
	}
	
	
//	public ArrayList<Monosaccharide> getMonosaccharides() {
//		return monosaccharides;
//	}
//
//	public void setMonosaccharides(ArrayList<Monosaccharide> monosaccharide) {
//		this.monosaccharides = monosaccharide;
//	}
//
//	public void addToMonosaccharides(Monosaccharide monosaccharide) {
//		this.monosaccharides.add(monosaccharide);
//	}
	
	
	public ArrayList<MonosaccharideModifier> getModifiers() {
		return modifiers;
	}
	
	public void setModifiers(ArrayList<MonosaccharideModifier> modifiers) {
		this.modifiers = modifiers;
	}
	
	public void addToModifiers(MonosaccharideModifier modifier) {
		this.modifiers.add(modifier);
	}
	
	public String getRingClosureStart() {
		return RingClosureStart;
	}

	public void setRingClosureStart(String ringClosureStart) {
		RingClosureStart = ringClosureStart;
	}

	public String getRingClosureEnd() {
		return RingClosureEnd;
	}

	public void setRingClosureEnd(String ringClosureEnd) {
		RingClosureEnd = ringClosureEnd;
	}
	
	
	/*Methods*/
	public void setRingClosureDefault(Superclass superClass)
	{
		if(superClass.getSymbol().equals("HEX"))
		{
		setRingClosureStart("1");
		setRingClosureEnd("5");
		}
		
		if(superClass.getSymbol().equals("PEN"))
		{
		setRingClosureStart("1");
		setRingClosureEnd("5");
		}
		if(superClass.getSymbol().equals("NON"))
		{
		setRingClosureStart("2");
		setRingClosureEnd("6");
		}
		if(superClass.getSymbol().equals("OCT"))
		{
			setRingClosureStart("2");
			setRingClosureEnd("6");
		}
        if(superClass.getSymbol().equals("TET"))
        {
            setRingClosureStart("1");
            setRingClosureEnd("4");
        }
		if(superClass.getSymbol().equals("HEP"))
		{
			setRingClosureStart("1");
			setRingClosureEnd("5");
		}
		
	}
	
	@Override
	public String toString() {
//		System.out.println("MonosaccharideResidue.toString() : " );
//		System.out.println("	Anomer : " + this.anomer.getSymbol() );
//		System.out.println("	Stereo : " + this.monosaccharide.getStereo().getSymbol() );
//		System.out.println("	Stem : " + this.monosaccharide.getStem().getSymbol() );
//		System.out.println("	Stem superclass: " + this.monosaccharide.getStem().getSuperclass() );
//		System.out.println("	Ring closure: " + this.RingClosureStart + Constants.COLON + this.RingClosureEnd );
		
		return super.toString();
	}
	
	public String getCTvalue()
	{
		String result = "";
		
		result = this.getResNb()	
					+ Constants.BASE_TYPE_MONOSAC //+ "b" 
					+ Constants.COLON 
					+ this.getAnomer().getSymbol(); 
		
		//
		if(this.monosaccharide.getStem() != null && this.monosaccharide.getStem().getSymbol().length()!=0 )			
		{
			result += Constants.DASH
					+ this.monosaccharide.getStereo().getSymbol()
					+ this.monosaccharide.getStem().getSymbol();
		}
		
		result += Constants.DASH
					+ this.monosaccharide.getSuperClass().getSymbol()
					+ Constants.DASH 
					+ this.getRingClosureStart()
					+ Constants.COLON 
					+ this.getRingClosureEnd();
					
		for(int i=0;i<modifiers.size();i++)
			{
			result += Constants.PIPE
					+ this.modifiers.get(i).getPosition()
					+ Constants.COLON  
					+ this.modifiers.get(i).getModifier().getSymbol() ;
			}
//		System.out.println("GenericMonosaccharideResidue getCTvalue : " + result);			
					
		return result;
	}
}
