package org.expasy.sugarconverter.residue;

import java.util.ArrayList;

public class GenericComposedResidue extends AbstractResidue {

	public GenericComposedResidue()
	{}

	
	public GenericComposedResidue(GenericMonosaccharideResidue mon, ArrayList<GenericSubstituentResidue> subs)
	{
		this.setMonosaccharide(mon);
		this.setSubstituents(subs);		
	}

	
	private GenericMonosaccharideResidue monosaccharide = new GenericMonosaccharideResidue();
	private ArrayList<GenericSubstituentResidue> substituents = new ArrayList<GenericSubstituentResidue>();

	
	
	public GenericMonosaccharideResidue getMonosaccharide() {
		return monosaccharide;
	}
	public void setMonosaccharide(GenericMonosaccharideResidue monosaccharide) {
		this.monosaccharide = monosaccharide;
	}

	public ArrayList<GenericSubstituentResidue> getSubstituents() {
		return substituents;
	}
	
	public void setSubstituents(ArrayList<GenericSubstituentResidue> substituents) {
		this.substituents = substituents;
	}
	
	public void addToSubstituents(GenericSubstituentResidue substituent) {
		this.substituents.add(substituent);
	}

	@Override
	public String toString() {
		monosaccharide.toString();
		for(int i = 0;i<substituents.size();i++)
		{
			substituents.get(i).toString();
		}
		return super.toString();
	}


}
