package org.expasy.sugarconverter.residue.iupac.composed.acetamido;


import org.expasy.sugarconverter.residue.*;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono.DFuc;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class DFucNAc extends GenericComposedResidue{

	public DFucNAc()
	{
		GenericMonosaccharideResidue dFuc = new DFuc();
		GenericSubstituentResidue nac = new NAc();
		Monosaccharide m = dFuc.getMonosaccharide();
		m.setStereo(StereoConfig.D);
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(dFuc);
		super.addToSubstituents(nac);
	}
	
	
	
}
