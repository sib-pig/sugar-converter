package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Pyr extends GenericSubstituentResidue{
//	private Substituent substituent=Substituent.Sulfate;
	public Pyr()
	{
		super.setSubstituent(Substituent.Pyruvate);
	}
}
