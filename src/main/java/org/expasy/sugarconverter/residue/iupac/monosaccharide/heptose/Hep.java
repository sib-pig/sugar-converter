package org.expasy.sugarconverter.residue.iupac.monosaccharide.heptose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;

public class Hep extends GenericMonosaccharideResidue{


	public Hep()
	{
		Monosaccharide m = new Monosaccharide(Stem.Hep);
		//m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
	}

}
