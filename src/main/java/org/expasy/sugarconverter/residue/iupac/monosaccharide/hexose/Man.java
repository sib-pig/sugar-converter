package org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.sugar.*;

public class Man extends GenericMonosaccharideResidue{

	public Man ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Man);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
	}
	
	
	
}
