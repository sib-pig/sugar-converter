package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Ido;
import org.expasy.sugarconverter.sugar.Modifier;

public class IdoA extends Ido {
	
	public IdoA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
