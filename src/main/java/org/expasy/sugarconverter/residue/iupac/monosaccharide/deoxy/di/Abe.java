package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.di;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Ara;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Xyl;
import org.expasy.sugarconverter.sugar.Modifier;

public class Abe extends Xyl {

	public Abe()
	{
		Monosaccharide m = getMonosaccharide();
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 3));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
}
