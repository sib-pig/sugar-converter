package org.expasy.sugarconverter.residue.iupac.composed.acetamido;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LIdo;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class LIdoNAc extends GenericComposedResidue{

	public LIdoNAc()
	{
		GenericMonosaccharideResidue lIdo = new LIdo();
		GenericSubstituentResidue nac = new NAc();

		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);

		super.setMonosaccharide(lIdo);
		super.addToSubstituents(nac);
	}
	
	
	
}
