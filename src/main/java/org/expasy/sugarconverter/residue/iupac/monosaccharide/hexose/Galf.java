package org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;

public class Galf  extends Gal {

	public Galf ()
	{
		Monosaccharide m = getMonosaccharide();
//		m.setSuperClass(Superclass.Pen);
		
		super.setMonosaccharide(m);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");
	}
}
