package org.expasy.sugarconverter.residue.iupac.composed.acetamido;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LAlt;
import org.expasy.sugarconverter.residue.iupac.substituent.N;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class LAltNAc extends GenericComposedResidue{

	public LAltNAc()
	{
		GenericMonosaccharideResidue lAlt = new LAlt();
		GenericSubstituentResidue nac = new NAc();

		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);

		super.setMonosaccharide(lAlt);
		super.addToSubstituents(nac);
	}
	
	
	
}
