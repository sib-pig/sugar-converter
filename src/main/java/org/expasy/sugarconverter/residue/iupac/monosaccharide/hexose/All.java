package org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class All extends GenericMonosaccharideResidue{

	public All()
	{
		Monosaccharide m = new Monosaccharide(Stem.All);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		
	}
	
	
	
}
