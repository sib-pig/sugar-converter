package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;

public class Pentf extends GenericMonosaccharideResidue{


	public Pentf()
	{
		Monosaccharide m = new Monosaccharide(Stem.Pent);
		//m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");
	}
}
