package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Man;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class DRha extends Man {

	public DRha()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
	
}
