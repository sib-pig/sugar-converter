package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.di;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Xyl;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Col extends Xyl {

	public Col()
	{
		Monosaccharide m = getMonosaccharide();
		super.setMonosaccharide(m);
		m.setStereo(StereoConfig.L);
	}
}
