package org.expasy.sugarconverter.residue.iupac.monosaccharide.heptose;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class LGroMan extends GroMan{

	// found on http://www.monosaccharidedb.org/display_monosaccharide.action?scheme=bcsdb&name=aD3dlyxHepp-ulosaric
	public LGroMan()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		

	}
	
}
