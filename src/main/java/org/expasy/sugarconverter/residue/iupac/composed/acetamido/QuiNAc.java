package org.expasy.sugarconverter.residue.iupac.composed.acetamido;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono.Qui;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Man;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;

public class QuiNAc extends GenericComposedResidue{

	public QuiNAc()
	{
		GenericMonosaccharideResidue qui = new Qui();
		GenericSubstituentResidue nac = new NAc();
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(qui);
		super.addToSubstituents(nac);
	}
	
}
