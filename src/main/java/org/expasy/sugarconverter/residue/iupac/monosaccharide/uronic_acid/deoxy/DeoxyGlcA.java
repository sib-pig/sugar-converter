package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid.deoxy;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid.GlcA;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyGlcA extends GlcA {

	public DeoxyGlcA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 0));

	}
}
