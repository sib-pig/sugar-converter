package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Hex;
import org.expasy.sugarconverter.sugar.Modifier;

public class HexA extends Hex {

	public HexA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
