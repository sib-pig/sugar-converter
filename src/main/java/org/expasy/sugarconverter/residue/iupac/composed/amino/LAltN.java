package org.expasy.sugarconverter.residue.iupac.composed.amino;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.All;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LAlt;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class LAltN extends GenericComposedResidue{

	public LAltN()
	{
		GenericMonosaccharideResidue lAlt = new LAlt();
		GenericSubstituentResidue amino = new N();

		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);

		super.setMonosaccharide(lAlt);
		super.addToSubstituents(amino);
	}
	
	
	
}
