package org.expasy.sugarconverter.residue.iupac.composed.amino;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.All;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Man;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class AllN extends GenericComposedResidue{

	public AllN()
	{
		GenericMonosaccharideResidue all = new All();
		GenericSubstituentResidue amino = new N();

		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);

		super.setMonosaccharide(all);
		super.addToSubstituents(amino);
	}
	
	
	
}
