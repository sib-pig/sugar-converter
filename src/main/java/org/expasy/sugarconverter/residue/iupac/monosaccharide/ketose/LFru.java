package org.expasy.sugarconverter.residue.iupac.monosaccharide.ketose;



import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class LFru extends Fru {

	public LFru()
	{

		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		super.setRingClosureStart("2");
		super.setRingClosureEnd("6");

	}
	
	
	
}
