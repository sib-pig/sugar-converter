package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class DFuc extends LFuc {

	public DFuc()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.D);
		setMonosaccharide(m);


	}
}
