package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.di;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Ara;
import org.expasy.sugarconverter.sugar.Modifier;

public class Oli extends Ara {

	public Oli()
	{
		Monosaccharide m = getMonosaccharide();
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 2));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
}
