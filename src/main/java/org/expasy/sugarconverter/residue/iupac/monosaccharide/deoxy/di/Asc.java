package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.di;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Asc extends Tyv {

	public Asc()
	{
		Monosaccharide m = getMonosaccharide();
		super.setMonosaccharide(m);
		m.setStereo(StereoConfig.L);
	}
}
