package org.expasy.sugarconverter.residue.iupac.composed.acetamido;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono.LFuc;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono.LRha;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class LRhaNAc extends GenericComposedResidue{

	public LRhaNAc()
	{
		GenericMonosaccharideResidue lRha = new LRha();
		GenericSubstituentResidue nac = new NAc();
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(lRha);
		super.addToSubstituents(nac);
	}
	
	
	
}
