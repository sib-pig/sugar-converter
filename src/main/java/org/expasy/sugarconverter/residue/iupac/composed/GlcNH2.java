package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.composed.amino.GlcN;

public class GlcNH2 extends GlcN {

	public GlcNH2()
	{

        Link linkN = new Link("3","1");
        super.getMonosaccharide().setLinkToPrevious(linkN);
	}
	
	
	
}
