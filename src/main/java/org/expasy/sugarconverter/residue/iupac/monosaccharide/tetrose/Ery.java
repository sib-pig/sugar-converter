package org.expasy.sugarconverter.residue.iupac.monosaccharide.tetrose;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class Ery extends GenericMonosaccharideResidue{

	public Ery()
	{
		Monosaccharide m = new Monosaccharide(Stem.Ery);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		//super.setStem(Stem.Ara);
		
	}
	
	
	
}
