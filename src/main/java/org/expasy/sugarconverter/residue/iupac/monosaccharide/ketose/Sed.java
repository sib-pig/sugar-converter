package org.expasy.sugarconverter.residue.iupac.monosaccharide.ketose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Sed extends GenericMonosaccharideResidue {


	public Sed()
	{
		Monosaccharide m = new Monosaccharide(Stem.Sed);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
		super.setRingClosureStart("2");
		super.setRingClosureEnd("6");
	}
}
