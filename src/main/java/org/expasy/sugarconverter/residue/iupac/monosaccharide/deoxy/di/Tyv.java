package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.di;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Ara;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Tyv  extends Ara {

	public Tyv ()
	{
		Monosaccharide m = getMonosaccharide();
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 3));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
}
