package org.expasy.sugarconverter.residue.iupac.composed;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Glc;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;
import org.expasy.sugarconverter.sugar.Modifier;

public class BacAc2 extends GenericComposedResidue{
	//TODO : set ac1 and ac2 position on position 2 and 4
	public BacAc2()
	{

		GenericMonosaccharideResidue glc = new Glc();
		GenericSubstituentResidue ac1 = new NAc();
		GenericSubstituentResidue ac2 = new NAc();
		
		glc.addToModifiers(new MonosaccharideModifier(Modifier.D, 2));
		glc.addToModifiers(new MonosaccharideModifier(Modifier.D, 4));
		glc.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
		
		Link linkAc1 = new Link("2","1");
		Link linkAc2 = new Link("4","1");
		ac1.setLinkToPrevious(linkAc1);
		ac2.setLinkToPrevious(linkAc2);
		
		super.setMonosaccharide(glc);
		super.addToSubstituents(ac1);
		super.addToSubstituents(ac2);
	}
}
