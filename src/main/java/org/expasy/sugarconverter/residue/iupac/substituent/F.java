package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class F extends GenericSubstituentResidue{
	
//	private Substituent substituent=Substituent.Unknown;
	
	public F ()
	{
//		System.err.println("Phosphate : " + super.getClass());
//		System.err.println("isGenericMonosaccharideResidue : " + isGenericMonosaccharideResidue());
//		System.err.println("isGenericSubstituentResidue : " + isGenericSubstituentResidue());
//		System.err.println("isGenericComposedResidue : " + isGenericComposedResidue());
		super.setSubstituent(Substituent.Fluoro);
	}
}
