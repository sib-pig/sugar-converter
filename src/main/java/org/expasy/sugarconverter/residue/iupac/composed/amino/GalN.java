package org.expasy.sugarconverter.residue.iupac.composed.amino;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class GalN extends GenericComposedResidue{

	public GalN()
	{
		GenericMonosaccharideResidue gal = new Gal();
		GenericSubstituentResidue amino = new N();

		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);

		super.setMonosaccharide(gal);
		super.addToSubstituents(amino);
	}
	
	
	
}
