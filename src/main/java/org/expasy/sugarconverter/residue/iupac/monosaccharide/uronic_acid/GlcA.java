package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Glc;
import org.expasy.sugarconverter.sugar.Modifier;

public class GlcA extends Glc {
	
	public GlcA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}

}
