package org.expasy.sugarconverter.residue.iupac.composed.amino;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LAlt;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LIdo;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class LIdoN extends GenericComposedResidue{

	public LIdoN()
	{
		GenericMonosaccharideResidue lIdo = new LIdo();
		GenericSubstituentResidue amino = new N();

		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);

		super.setMonosaccharide(lIdo);
		super.addToSubstituents(amino);
	}
	
	
	
}
