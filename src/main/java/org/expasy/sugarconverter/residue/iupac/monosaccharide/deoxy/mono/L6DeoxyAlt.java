package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Alt;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class L6DeoxyAlt extends Alt {

	public L6DeoxyAlt()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));

	}
	
	
	
}
