package org.expasy.sugarconverter.residue.iupac.composed.acetamido;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gul;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;


public class GulNAc extends GenericComposedResidue{

	public GulNAc()
	{
		GenericMonosaccharideResidue gul = new Gul();
		GenericSubstituentResidue nac = new NAc();

		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);

		super.setMonosaccharide(gul);
		super.addToSubstituents(nac);
	}
	
	
	
}
