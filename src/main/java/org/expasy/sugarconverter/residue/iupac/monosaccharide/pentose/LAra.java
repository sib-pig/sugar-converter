package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class LAra extends Ara{

	public LAra()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);

		
	}
	
	
	
}
