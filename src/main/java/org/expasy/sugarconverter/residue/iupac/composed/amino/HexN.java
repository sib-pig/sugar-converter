package org.expasy.sugarconverter.residue.iupac.composed.amino;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Hex;
import org.expasy.sugarconverter.residue.iupac.substituent.N;

public class HexN extends GenericComposedResidue{

	public HexN()
	{
		GenericMonosaccharideResidue hex = new Hex();
		GenericSubstituentResidue amino = new N();
		
		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(hex);
		super.addToSubstituents(amino);
	}

}
