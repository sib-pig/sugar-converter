package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LAlt;
import org.expasy.sugarconverter.sugar.Modifier;

public class LAltA extends LAlt {

	public LAltA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
