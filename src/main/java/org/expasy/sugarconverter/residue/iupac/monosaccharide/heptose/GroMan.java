package org.expasy.sugarconverter.residue.iupac.monosaccharide.heptose;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class GroMan extends GenericMonosaccharideResidue{

	// found on http://www.monosaccharidedb.org/display_monosaccharide.action?scheme=bcsdb&name=aXLDmanHepp
	public GroMan()
	{
		Monosaccharide m = new Monosaccharide(Stem.GroManHep);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 1));
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 3));
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 7));
	}
	
}
