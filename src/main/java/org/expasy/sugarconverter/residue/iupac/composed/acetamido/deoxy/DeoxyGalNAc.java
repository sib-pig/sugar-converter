package org.expasy.sugarconverter.residue.iupac.composed.acetamido.deoxy;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.composed.acetamido.GalNAc;
import org.expasy.sugarconverter.sugar.Modifier;


public class DeoxyGalNAc extends GalNAc {

	public DeoxyGalNAc()
	{
        super.getMonosaccharide().addToModifiers(new MonosaccharideModifier(Modifier.D, 0));
	}
	
	
	
}
