package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.Monosaccharide;


public class Ribf extends Rib{

	public Ribf()
	{
		Monosaccharide m = getMonosaccharide();

		super.setMonosaccharide(m);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");

		
	}
	
	
	
}
