package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Man;
import org.expasy.sugarconverter.sugar.Modifier;

public class ManA  extends Man {
	
	public ManA ()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
