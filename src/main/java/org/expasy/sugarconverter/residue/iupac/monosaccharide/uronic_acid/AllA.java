package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.All;
import org.expasy.sugarconverter.sugar.Modifier;

public class AllA extends All {

	public AllA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
