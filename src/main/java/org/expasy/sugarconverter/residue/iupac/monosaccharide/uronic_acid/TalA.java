package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Tal;
import org.expasy.sugarconverter.sugar.Modifier;

public class TalA extends Tal {

	public TalA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 6));

	}
}
