package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class Araf extends Ara{

	public Araf()
	{
		Monosaccharide m = getMonosaccharide();

		super.setMonosaccharide(m);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");

		
	}
	
	
	
}
