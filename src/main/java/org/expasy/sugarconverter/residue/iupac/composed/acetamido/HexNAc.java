package org.expasy.sugarconverter.residue.iupac.composed.acetamido;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Hex;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;

public class HexNAc extends GenericComposedResidue{

	public HexNAc()
	{
		GenericMonosaccharideResidue hex = new Hex();
		GenericSubstituentResidue nac = new NAc();
		
		Link linkAc = new Link("2","1");
		nac.setLinkToPrevious(linkAc);
		
		super.setMonosaccharide(hex);
		super.addToSubstituents(nac);
	}

}
