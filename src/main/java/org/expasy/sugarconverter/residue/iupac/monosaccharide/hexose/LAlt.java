package org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class LAlt extends Alt{

	public LAlt()
	{
        Monosaccharide m = getMonosaccharide();
        m.setStereo(StereoConfig.L);
        super.setMonosaccharide(m);

		
	}
	
	
	
}
