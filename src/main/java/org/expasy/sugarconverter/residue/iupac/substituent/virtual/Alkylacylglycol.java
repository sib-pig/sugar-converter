package org.expasy.sugarconverter.residue.iupac.substituent.virtual;

import org.expasy.sugarconverter.residue.GenericVirtualSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Alkylacylglycol extends GenericVirtualSubstituentResidue{
        
        
        public Alkylacylglycol ()
        {
                super.setSubstituent(Substituent.Alkylacylglycol);
        }
}

