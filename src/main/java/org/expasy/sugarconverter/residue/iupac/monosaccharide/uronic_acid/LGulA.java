package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gul;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class LGulA extends GulA {

	public LGulA()
	{

		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
	}
}
