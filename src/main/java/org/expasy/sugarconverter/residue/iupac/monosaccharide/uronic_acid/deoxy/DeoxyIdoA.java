package org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid.deoxy;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.uronic_acid.IdoA;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyIdoA extends IdoA {

	public DeoxyIdoA()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 0));

	}
}
