package org.expasy.sugarconverter.residue.iupac.composed.amino.deoxy;


import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.composed.amino.GalN;
import org.expasy.sugarconverter.sugar.Modifier;


public class DeoxyGalN extends GalN {

	public DeoxyGalN()
	{
        super.getMonosaccharide().addToModifiers(new MonosaccharideModifier(Modifier.D, 0));
	}
	
	
	
}
