package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class LAraf extends Ara{

	public LAraf()
	{
		Monosaccharide m = getMonosaccharide();

		super.setMonosaccharide(m);
		m.setStereo(StereoConfig.L);
		this.setRingClosureStart("1");
		this.setRingClosureEnd("4");

		
	}
	
	
	
}
