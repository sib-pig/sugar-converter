package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;



import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gal;
import org.expasy.sugarconverter.sugar.*;

public class LFuc extends Gal {

	
	public LFuc()
	{
		//super.addToMonosaccharides(new Monosaccharide(Stem.Gal));
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		this.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}

}
