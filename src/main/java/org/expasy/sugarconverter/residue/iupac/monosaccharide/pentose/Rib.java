package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class Rib extends GenericMonosaccharideResidue{

	public Rib()
	{
		Monosaccharide m = new Monosaccharide(Stem.Rib);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		//super.setStem(Stem.Ara);
		
	}
	
	
	
}
