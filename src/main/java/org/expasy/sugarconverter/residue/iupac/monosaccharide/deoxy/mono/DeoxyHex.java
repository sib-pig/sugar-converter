package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Hex;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyHex extends Hex {
	public DeoxyHex()
	{
		//Monosaccharide m = getMonosaccharide();
		//m.setStereo(StereoConfig.L);
		//super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
	}
}
