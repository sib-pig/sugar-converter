package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.*;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;


public class Ara extends GenericMonosaccharideResidue{

	public Ara ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Ara);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		//super.setStem(Stem.Ara);
		
	}
	
	
	
}
