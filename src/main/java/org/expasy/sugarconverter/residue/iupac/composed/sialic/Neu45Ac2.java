package org.expasy.sugarconverter.residue.iupac.composed.sialic;

import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.keto_ulosonic_acid.Kdn;
import org.expasy.sugarconverter.residue.iupac.substituent.NAc;

public class Neu45Ac2  extends GenericComposedResidue{
	//TODO : set ac1 and ac2 position
	public Neu45Ac2()
	{

		GenericMonosaccharideResidue neu = new Kdn();
		GenericSubstituentResidue ac1 = new NAc();
		GenericSubstituentResidue ac2 = new NAc();
		
		Link linkAc1 = new Link("4","1");
		Link linkAc2 = new Link("5","1");
		ac1.setLinkToPrevious(linkAc1);
		ac2.setLinkToPrevious(linkAc2);
		
		super.setMonosaccharide(neu);
		super.addToSubstituents(ac1);
		super.addToSubstituents(ac2);
	}
}
