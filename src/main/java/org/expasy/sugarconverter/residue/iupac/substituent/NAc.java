package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class NAc extends GenericSubstituentResidue{
	
	//private Substituent substituent=Substituent.Acetyl;
	
	public NAc ()
	{
		super.setSubstituent(Substituent.NAcetyl);
	}
}
