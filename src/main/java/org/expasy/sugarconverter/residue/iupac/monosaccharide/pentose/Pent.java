package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;

public class Pent extends GenericMonosaccharideResidue{

	
	public Pent ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Pent);
		//m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
	}
}
