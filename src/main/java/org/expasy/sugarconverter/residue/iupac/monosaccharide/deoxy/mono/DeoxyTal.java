package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.mono;

import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Glc;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Tal;
import org.expasy.sugarconverter.sugar.Modifier;

public class DeoxyTal extends Tal {

	public DeoxyTal()
	{
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));

	}
}
