package org.expasy.sugarconverter.residue.iupac.monosaccharide.ketose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Xyl;
import org.expasy.sugarconverter.sugar.*;

public class Sor extends GenericMonosaccharideResidue {

	
	public Sor ()
	{
		Monosaccharide m = new Monosaccharide(Stem.Sor);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
		super.setRingClosureStart("2");
		super.setRingClosureEnd("6");
	}
	
}
