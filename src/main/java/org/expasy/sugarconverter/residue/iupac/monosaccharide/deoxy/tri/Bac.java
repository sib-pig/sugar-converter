package org.expasy.sugarconverter.residue.iupac.monosaccharide.deoxy.tri;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Glc;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Bac extends Glc{
	//TODO : set ac1 and ac2 position on position 2 and 4
	public Bac()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 2));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 4));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 6));
		
	}
}
