package org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose;

import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Ido extends GenericMonosaccharideResidue {

	public Ido ()
	{
		
		Monosaccharide m = new Monosaccharide(Stem.Ido);
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
	}
}
