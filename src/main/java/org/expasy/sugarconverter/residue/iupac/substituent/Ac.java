package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Ac extends GenericSubstituentResidue{
	
	//private Substituent substituent=Substituent.Acetyl;
	
	public Ac ()
	{
		super.setSubstituent(Substituent.Acetyl);
	}
}
