package org.expasy.sugarconverter.residue.iupac.composed;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Glc;
import org.expasy.sugarconverter.residue.iupac.substituent.NS;


public class GlcNS extends GenericComposedResidue{

	public GlcNS()
	{
		GenericMonosaccharideResidue glc = new Glc();
		GenericSubstituentResidue ns = new NS();
		
		Link linkNs = new Link("2","1");
		ns.setLinkToPrevious(linkNs);
		
		super.setMonosaccharide(glc);
		super.addToSubstituents(ns);
	}
	
	
	
}
