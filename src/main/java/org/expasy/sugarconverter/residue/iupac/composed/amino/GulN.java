package org.expasy.sugarconverter.residue.iupac.composed.amino;


import org.expasy.sugarconverter.residue.GenericComposedResidue;
import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.residue.Link;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.Gul;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.hexose.LAlt;
import org.expasy.sugarconverter.residue.iupac.substituent.N;


public class GulN extends GenericComposedResidue{

	public GulN()
	{
		GenericMonosaccharideResidue gul = new Gul();
		GenericSubstituentResidue amino = new N();

		Link linkAc = new Link("2","1");
		amino.setLinkToPrevious(linkAc);

		super.setMonosaccharide(gul);
		super.addToSubstituents(amino);
	}
	
	
	
}
