package org.expasy.sugarconverter.residue.iupac.monosaccharide.ketose;

import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose.Xyl;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class LSor extends Sor {


	public LSor()
	{
		Monosaccharide m = getMonosaccharide();
		m.setStereo(StereoConfig.L);
		super.setMonosaccharide(m);
		super.setRingClosureStart("2");
		super.setRingClosureEnd("6");
	}
	
}
