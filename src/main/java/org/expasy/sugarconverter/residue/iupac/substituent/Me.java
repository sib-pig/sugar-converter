package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class Me extends GenericSubstituentResidue{
//	private Substituent substituent=Substituent.Methyl;
	
	public Me ()
	{
		super.setSubstituent(Substituent.Methyl);
	}
}
