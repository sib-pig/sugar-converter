package org.expasy.sugarconverter.residue.iupac.monosaccharide.keto_ulosonic_acid;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.residue.MonosaccharideModifier;
import org.expasy.sugarconverter.sugar.Modifier;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Kdo extends GenericMonosaccharideResidue{


	public Kdo()
	{
//		http://www.monosaccharidedb.org/display_monosaccharide.action?id=226
		Monosaccharide m = new Monosaccharide(Stem.Kdo);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
		
		super.addToModifiers(new MonosaccharideModifier(Modifier.A, 1));
		super.addToModifiers(new MonosaccharideModifier(Modifier.Keto, 2));
		super.addToModifiers(new MonosaccharideModifier(Modifier.D, 3));
	}
	
}
