package org.expasy.sugarconverter.residue.iupac.monosaccharide.pentose;


import org.expasy.sugarconverter.residue.GenericMonosaccharideResidue;
import org.expasy.sugarconverter.residue.Monosaccharide;
import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;

public class Lyx extends GenericMonosaccharideResidue{


	public Lyx()
	{
		Monosaccharide m = new Monosaccharide(Stem.Lyx);
		m.setStereo(StereoConfig.D);
		super.setMonosaccharide(m);
	}
	
//	private Monosaccharide monsaccharide = new Monosaccharide();
//	
//	protected void init()
//	{
//		MonosaccharideResidue
//	}
	
	
	
}
