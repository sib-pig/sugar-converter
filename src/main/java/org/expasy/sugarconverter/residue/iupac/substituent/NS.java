package org.expasy.sugarconverter.residue.iupac.substituent;

import org.expasy.sugarconverter.residue.GenericSubstituentResidue;
import org.expasy.sugarconverter.sugar.Substituent;

public class NS extends GenericSubstituentResidue{

	//private Substituent substituent=Substituent.Acetyl;

	public NS()
	{
		super.setSubstituent(Substituent.NSulfate);
	}
}
