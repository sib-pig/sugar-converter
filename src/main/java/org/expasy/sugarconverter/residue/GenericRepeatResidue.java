package org.expasy.sugarconverter.residue;

import org.expasy.sugarconverter.parser.Constants;
import org.expasy.sugarconverter.utils.TreeTools;

public class GenericRepeatResidue  extends AbstractResidue{
	
	private int index;


	/*Constructor */	
	public GenericRepeatResidue()
	{
		
	}
	
	/*Accessor */
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	/*Methods */
	public String getCTvalue()
	{
		
		String result = "";
		
		result = this.getResNb()	
					+ Constants.BASE_TYPE_REPEAT //+ "r" 
					+ Constants.COLON 
					+ Constants.BASE_TYPE_REPEAT
					+ this.index;
//		TreeTools.getTopTree(this.getIupacResidue().getBranch().getTree()).
					;
					 
		System.out.println("GenericRepeatResidue getCTvalue : " + result);			
					
		return result;
	}
}
