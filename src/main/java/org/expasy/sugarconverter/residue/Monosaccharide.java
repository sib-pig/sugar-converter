package org.expasy.sugarconverter.residue;

import org.expasy.sugarconverter.sugar.Stem;
import org.expasy.sugarconverter.sugar.StereoConfig;
import org.expasy.sugarconverter.sugar.Superclass;

public class Monosaccharide {
	
	/**Constructor for Monosaccharide with usual Superclass associated to Stem as defined in Enum Stem */
	public Monosaccharide (Stem stem)
	{
		this.stem = stem;
		this.superClass = stem.getSuperclass(); //fromString(stem.getSymbol()).getSuperclass();
	}
	
	/**Constructor for Monosaccharide with different Superclass as the usual associated to Stem */
	public Monosaccharide (Stem stem, Superclass superClass)
	{
		this.stem = stem;
		this.superClass = superClass;
	}
	
	private StereoConfig stereo = StereoConfig.X;
	private Stem stem;
	private Superclass superClass;
	
	public Superclass getSuperClass() {
		return superClass;
	}
	public void setSuperClass(Superclass superClass) {
		this.superClass = superClass;
	}
	public StereoConfig getStereo() {
		return stereo;
	}
	
	public void setStereo(StereoConfig stereo) {
		this.stereo = stereo;
	}
	
	public void setStereo(String stereo) {
		this.stereo = StereoConfig.fromString(stereo);
	}
	
	public Stem getStem() {
		return stem;
	}
	
	public void setStem(Stem stem) {
		this.stem = stem;
	}
	

}
