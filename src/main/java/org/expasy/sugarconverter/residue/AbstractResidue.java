package org.expasy.sugarconverter.residue;

import org.expasy.sugarconverter.parser.IupacResidue;
import org.expasy.sugarconverter.residue.iupac.substituent.Me;
import org.expasy.sugarconverter.residue.iupac.substituent.S;

public class AbstractResidue {

	private int resNb=0;	
	private Link linkToPrevious=null;
	private int id;
	private IupacResidue iupacResidue =null;
	


	public Link getLinkToPrevious() {
		return linkToPrevious;
	}

	public void setLinkToPrevious(Link linkToPrevious) {
		this.linkToPrevious = linkToPrevious;
	}
	
	public int getResNb() {
		return resNb;
	}

	public void setResNb(int resNb) {
		this.resNb = resNb;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public IupacResidue getIupacResidue() {
		return iupacResidue;
	}

	public void setIupacResidue(IupacResidue iupacResidue) {
		this.iupacResidue = iupacResidue;
	}
	
	public boolean isCreated()
	{
//		System.out.println("AbstractResidue : Created");
		return true;
	}
	
	public boolean isGenericComposedResidue()
	{
		boolean result = false;
		GenericComposedResidue c = new GenericComposedResidue();
		
		if(c.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericComposedResidue " + result);
		}
		return result;
	}
	
	public boolean isGenericMonosaccharideResidue()
	{
		boolean result = false;
		GenericMonosaccharideResidue m = new GenericMonosaccharideResidue();
		
		if(m.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericMonosaccharideResidue " + result);
		}
	
		return result;
	}
	
	public boolean isGenericSubstituentResidue()
	{
		boolean result = false;
		GenericSubstituentResidue s = new GenericSubstituentResidue();
		
		if(s.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericSubstituentResidue " + result);
		}
		
		return result;
	}
	
	
	public boolean isGenericRepeatResidue()
	{
		boolean result = false;
		GenericRepeatResidue r = new GenericRepeatResidue();
		
		if(r.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericRepeatResidue " + result);
		}
		
		return result;
	}
	
	public boolean isSulfateSubstituentResidue()
	{
		boolean result = false;
		S sulfate = new S();
		
		if(sulfate.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericSubstituentResidue " + result);
		}
		
		return result;
	}

	public boolean isMethylSubstituentResidue()
	{
		boolean result = false;
		Me methyl = new Me();

		if(methyl.getClass().isAssignableFrom(this.getClass()))
		{
			result=true;
//			System.out.println("AbstractResidue : isGenericSubstituentResidue " + result);
		}

		return result;
	}

}
