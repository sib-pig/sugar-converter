package org.expasy.sugarconverter.comparator;

import java.util.ArrayList;

import org.expasy.sugarconverter.parser.IupacBranch;

public class CtNodeComparator extends AbstractComparator {
	
	/*Constructor*/
	public CtNodeComparator(ArrayList<IupacBranch> branches) 
	{
		super(branches);
	}

	/*Methods*/
	public ArrayList<IupacBranch> compare()
	{
		//TODO : no comparison performed on node comparator.
		setBranchesCompared(this.getBranchesToCompare());
		
		return this.getBranchesCompared();
	}
}
