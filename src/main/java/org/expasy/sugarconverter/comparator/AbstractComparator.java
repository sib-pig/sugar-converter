package org.expasy.sugarconverter.comparator;

import java.util.ArrayList;

import org.expasy.sugarconverter.parser.IupacBranch;

public abstract class AbstractComparator {

	private ArrayList<IupacBranch> branchesToCompare;
	private ArrayList<IupacBranch> branchesCompared = new ArrayList<IupacBranch>();
	
	/*Constructor*/
	public AbstractComparator(ArrayList<IupacBranch> branches)
	{
		this.branchesToCompare = branches;		
	}
	
	/*Getters,setters*/
	public ArrayList<IupacBranch> getBranchesToCompare() {
		return branchesToCompare;
	}


	public void setBranchesToCompare(ArrayList<IupacBranch> branchesToCompare) {
		this.branchesToCompare = branchesToCompare;
	}


	public ArrayList<IupacBranch> getBranchesCompared() {
		return branchesCompared;
	}


	public void setBranchesCompared(ArrayList<IupacBranch> branchesCompared) {
		this.branchesCompared = branchesCompared;
	}
}
