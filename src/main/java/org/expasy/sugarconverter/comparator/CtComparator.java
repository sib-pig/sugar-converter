package org.expasy.sugarconverter.comparator;

import java.util.ArrayList;

import org.expasy.sugarconverter.parser.IupacBranch;
import org.expasy.sugarconverter.utils.TreeTools;

public class CtComparator extends AbstractComparator {
	
	/*Constructor*/
	public CtComparator(ArrayList<IupacBranch> branches) 
	{
		super(branches);
		
//		System.out.println("CtComparator : new CtComparator()");
//		System.out.println("CtComparator : new CtComparator() branches : " + TreeTools.arrayIdInLine(branches));
		
	}
	
	/*Methods*/
	public IupacBranch compare()
	{
//		IupacBranch branchSelected
		ArrayList<IupacBranch> branchesEdgeComp = new ArrayList<IupacBranch>();
		ArrayList<IupacBranch> branchesLinkageComp = new ArrayList<IupacBranch>();
		
//		System.out.println("CtComparator compare : branchesEdgeComp.size() before comparison : " + branchesEdgeComp.size());
		
		try
		{
			/*Node comparator*/
			//TODO : Node comparator to complete traversal algorithm
			
			/*Edge comparator*/
			branchesEdgeComp = new CtEdgeComparator(this.getBranchesToCompare()).compare();
			this.setBranchesCompared(branchesEdgeComp);
			
			/*Linkage comparator*/
			if(branchesEdgeComp.size()!=1)
			{
				//branchesLinkageComp = branchesEdgeComp;
				branchesLinkageComp = new CtLinkageComparator(branchesEdgeComp).compare();
				this.setBranchesCompared(branchesLinkageComp);
			}
			
//			System.out.println("CtComparator compare : branchesLinkageComp.size() after comparison : " + branchesLinkageComp.size());
			
		}
		catch(Exception ex)
		{
			System.err.println("CtComparator compare() : " + ex.getMessage());
		}
		
		int index = 0; //branchesLinkageComp.size()-1 ;
		//System.out.println("CtComparator compare : selected branch : " + branchesLinkageComp.get(0).getId());
//		System.out.println("CtComparator compare : selected branch (index=" + index + ") : " + branchesLinkageComp.get(index).getId());
		
		return branchesLinkageComp.get(index);
		//return this.getBranchesToCompare().get(0);
		//return this.getBranchesCompared().get(0);
	}

}
