package org.expasy.sugarconverter.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import org.expasy.sugarconverter.parser.Constants;
import org.expasy.sugarconverter.parser.IupacParser;
import org.expasy.sugarconverter.parser.IupacTree;

public class DbConnection {
	
	private Statement stmtSelect = null;
	
	public Statement getStmtSelect() {
		return stmtSelect;
	}

	public void setStmtSelect(Statement stmtSelect) {
		this.stmtSelect = stmtSelect;
	}

	public void loadDriver()
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
			System.out.println("Class.forName(org.postgresql.Driver); : Driver loaded!");
		} 
		catch (ClassNotFoundException e) 
		{
			System.err.println(e.getMessage());
		}
	}
	
	public String getUrl(String dbName, String user, String password, boolean ssl, String sslfactory)
	{
//		String db = "GlycoSuiteDB";
//		String user = "julien";
//		String password = "000000";
//		boolean ssl = true;
//      String sslfactory="org.postgresql.ssl.NonValidatingFactory";
		
		String url = "jdbc:postgresql://localhost/"+dbName+"?user="+user+"&password="+password+"&ssl="+ssl+"&sslfactory="+sslfactory;
        System.out.println(url);
		return url;
	}
	
	public String getUrl(String dbName)
	{
//		String db = "GlycoSuiteDB";
		String user = "julien";
		String password = "000000";
		boolean ssl = true;
        String sslfactory="org.postgresql.ssl.NonValidatingFactory";
		
		String url = getUrl(dbName, user, password, ssl, sslfactory);//"jdbc:postgresql://localhost/"+dbName+"?user="+user+"&password="+password+"&ssl="+ssl;

        return url;
	}

	public ResultSet getData (String dbName, String querySelect)
	{
		
//		String query = "";
		ResultSet rs = null;
		try 
		{
			this.loadDriver();
			Connection conn = DriverManager.getConnection(getUrl(dbName, DbNames.user, DbNames.password, DbNames.ssl, DbNames.sslfactory));
	
//			if(dbName==DbNames.glycosuite)
//			{
//				query = DbNames.querySelectGS;
//				
//			}
			this.stmtSelect = conn.createStatement();
			rs = stmtSelect.executeQuery(querySelect);
		}
		catch(Exception ex)
		{
			System.err.println(ex.getMessage());
		}
		return rs;
	}
	
	
}
