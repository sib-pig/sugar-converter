package org.expasy.sugarconverter.parser;

public class IupacRepeatTree extends IupacTree {


	private int multitudeMax;
	private int multitudeMin;	
	private IupacResidue residue = null;

	

	/*Constructor */
	public IupacRepeatTree(String input) {
		//super.originalSequence=input;
		super(input);
	}
	
	protected IupacRepeatTree(String input, int multitudeMin, int multitudeMax) {
		super(input);
		this.multitudeMin = multitudeMin;
		this.multitudeMax = multitudeMax;
		
	}

	/*Accessor */
	public int getMultitudeMax() {
		return multitudeMax;
	}

	public void setMultitudeMax(int multitudeMax) {
		this.multitudeMax = multitudeMax;
	}

	public int getMultitudeMin() {
		return multitudeMin;
	}

	public void setMultitudeMin(int multitudeMin) {
		this.multitudeMin = multitudeMin;
	}
	
	public IupacResidue getResidue() {
		return residue;
	}

	public void setResidue(IupacResidue residue) {
		this.residue = residue;
	}
	

}
