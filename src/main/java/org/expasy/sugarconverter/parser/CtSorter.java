package org.expasy.sugarconverter.parser;

import java.util.ArrayList;
import org.expasy.sugarconverter.comparator.CtComparator;
import org.expasy.sugarconverter.utils.TreeTools;

public class CtSorter {
	

	private ArrayList<IupacBranch> branchesToSort;
	private ArrayList<IupacBranch> branchesSorted = new ArrayList<IupacBranch>();
	
	/*Constructor*/
	public CtSorter(ArrayList<IupacBranch> branches)
	{
		this.branchesToSort = branches;		
	}
	
	/*Getters,setters*/
	public ArrayList<IupacBranch> getBranchesToSort() {
		return branchesToSort;
	}

	public void setBranchesToSort(ArrayList<IupacBranch> branchesToSort) {
		this.branchesToSort = branchesToSort;
	}

	public ArrayList<IupacBranch> getBranchesSorted() {
		return branchesSorted;
	}

	public void setBranchesSorted(ArrayList<IupacBranch> branchesSorted) {
		this.branchesSorted = branchesSorted;
	}
	
	/*Methods*/
	public void sortBranches()
	{
		IupacBranch current = null;
		IupacBranch child = null;
		IupacBranch parent = null;
		ArrayList<IupacBranch> selectedChildren = null;
		
		/*get tree*/
		/*find root branch and set as current branch*/
		current = TreeTools.getRootBranch(branchesToSort);
		
			/*add to new sorted tree*/
			branchesSorted.add(current);
			
			while(branchesSorted.size() < branchesToSort.size())
			{
			/*get children  branches*/
//				System.out.println("CtSorter sortBranches current Id: "+ current.getId());
				
				/*if children not already in the sorted array*/
				selectedChildren = TreeTools.getChildrenBranchesNotInSortedTree(current, branchesToSort, branchesSorted);			
				if(selectedChildren.size()>0)
				{
					/*apply comparator*/
					child = compare(selectedChildren);
					/*add choosen  branch to new sorted tree*/
					branchesSorted.add(child);
//					System.out.println("CtSorter sortBranches child added Id: "+ child.getId());
					current = child;
				}
				/*if no children */
				else
				{
					/*go up to parent and set as current*/
					parent = TreeTools.getParentBranch(current, branchesToSort); 
//					System.out.println("CtSorter sortBranches parent Id: "+ parent.getId());
					
					current = parent;
//					System.out.println("CtSorter sortBranches current Id: "+ current.getId());
					
				}	
//				System.out.println("CtSorter sortBranches current Id: "+ current.getId());
//				System.out.println("CtSorter sortBranches branchesSorted.size(): "+ branchesSorted.size());	
//				System.out.println("CtSorter sortBranches branchesToSort.size(): "+ branchesToSort.size());
			}
		
	}
	
	public IupacBranch compare(ArrayList<IupacBranch> branches) //throws Exception
	{
		IupacBranch selectedBranch = null;
		
		if(branches.size()==1)
		{
			return branches.get(0);
		}
		
		try
		{
		CtComparator comp = new CtComparator(branches);
//		System.out.println("CtSorter compare : new CtComparator(branches)" );	
		selectedBranch = comp.compare();
//		System.out.println("CtSorter compare : selectedBranch " + selectedBranch);
		}
		catch(Exception ex)
		{
			System.err.println("CtSorter compare : " + ex.getMessage());	
			
		}
		return selectedBranch;
	}
	
	
}
