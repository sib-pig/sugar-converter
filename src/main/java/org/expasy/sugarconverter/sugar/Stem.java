package org.expasy.sugarconverter.sugar;
/**************************************************
*<p>  
*   
*   
*</p> 
* @author Julien Mariethoz
* 
*/
public enum Stem 
{
	//Stems with their "usual" superclass
	Gro("Glyceraldehyde","gro",Superclass.Tri),


	Ery("Erythrose","ery",Superclass.Tet),
	Thr("Threose","thr",Superclass.Tet),

	Rib("Ribose","rib",Superclass.Pen),
	Ara("Arabinose","ara",Superclass.Pen),
	Xyl("Xylose","xyl",Superclass.Pen),
	Lyx("Lyxose","lyx",Superclass.Pen),
	Xul("Xylulose","thr",Superclass.Pen),

	All("Allose","all",Superclass.Hex),
	Alt("Altrose","alt",Superclass.Hex),
	Glc("Glucose","glc",Superclass.Hex),
	Man("Mannose","man",Superclass.Hex),
	Gul("Gulose","gul",Superclass.Hex),
	Ido("Idose","ido",Superclass.Hex),
	Gal("Galactose","gal",Superclass.Hex),
	Tal("Talose","tal",Superclass.Hex),
	Ami("Amicetose","ery",Superclass.Hex),
	Fru("Fructose","ara",Superclass.Hex),
	Sor("Sorbose","xyl",Superclass.Hex),
	Tag("Tagatose","lyx",Superclass.Hex),

	Dha("Dha","lyx",Superclass.Hep),
	GroManHep("GroManHeptose","gro-dman",Superclass.Hep),
	Sed("Sedoheptulose","alt",Superclass.Hep),

	Kdo("Ketodeoxyoctulosonic acid","man",Superclass.Oct), //b-dman-OCT-2:6|1:a|2:keto|3:d
	Ko("Ketooctonic acid","gro-dtal",Superclass.Oct),

	//TODO : compose from gro and gal and their respective isomer
	Kdn("Ketodeoxynonulonic acid","gro-dgal",Superclass.Non),

	//Virtual
	Hex("Hexose","",Superclass.Hex),
	Pent("Pentose","",Superclass.Pen),
	Tri("Triose","",Superclass.Tri),
	Tet("Tetrose","",Superclass.Tet),
	Hep("Heptose","",Superclass.Hep),
	Oct("Octose","",Superclass.Oct),
	Non("Nonose","",Superclass.Non)
	;

    /** Stem name */
    private String fullname;
	/** Stem short name. */
    private String symbol;
    private Superclass superclass;
    
    
    


	/** Private constructor, see the forName methods for external use. */
    private Stem( String fullname, String symbol, Superclass superclass ) 
    { 
        this.fullname = fullname; 
        this.symbol = symbol; 
        this.superclass = superclass;
    }
     
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static Stem fromString(String symbol) {
        if (symbol != null) 
        {
          for (Stem s : Stem.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol)) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    public String getFullname() {
		return fullname;
	}

	public String getSymbol() {
		return symbol;
	}

	public Superclass getSuperclass() {
		return superclass;
	}

//	public void setSuperclass(String superclass) {
//		this.superclass = superclass;
//	}
    /*
    public static Stem forName( String stem ) 
    { 
        switch (stem)
        {
        case "gro": return Gro;
        case "ery": return Ery;
        case "rib": return Rib;
        case "ara": return Ara;
        case "all": return All;
        case "alt": return Alt;
        case "glc": return Glc;
        case "man": return Man;
        case "tre": return Tre;
        case "xyl": return Xyl;
        case "lyx": return Lyx;
        case "gul": return Gul;
        case "ido": return Ido;
        case "gal": return Gal;
        case "tal": return Tal;
        }
    	*/
    }