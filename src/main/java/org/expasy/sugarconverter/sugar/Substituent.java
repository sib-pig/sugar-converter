package org.expasy.sugarconverter.sugar;
public enum Substituent {

	Acetyl("COCH3","acetyl",1),
	Bromo("Br","bromo",1),
	Chloro("Cl","chloro",1),
	Ethyl("CH2CH3","ethyl",1),
	Ethanolamine("CH2NHCH2OH","ethanolamine",1),
	Fluoro("F","fluoro",1),
	Formyl("CHO","formyl",1),
	Glycolyl("COCH2OH","glycolyl",1),
	Hydroxymethyl("CH2OH","hydroxymethyl",1),
	Imino("NH","imino",2),
	Iodo("I","iodo",1),
	RLactate1("CH3CHCOOH","(r)-lactate",1),
	SLactate1("CH3CHCOOH","(s)-lactate",1),
	Methyl("CH3","methyl",1),
	N("NH2","amino",1),
	NAcetyl("NHCOCH3","n-acetyl",1),
	NAlanine("NHCOCHNH2CH3","n-alanine",1),
	NDimethyl("N(CH3)2","n-dimethyl",1),
	NFormyl("NHCHO","n-formyl",1),
	NGlycolyl("NCOCH2OH","n-glycolyl",1),
	NMethyl("NHCH3","n-methyl",1),
	NSuccinate("NCOCH2CH2COOH","n-succinate",1),
	NSulfate("NHSO3H","n-sulfate",1),
	NTriflouroacetyl("NHCOCF3","n-triflouroacetyl",1),
	Nitrat("NO2","nitrat",1),
	Phosphate("PO3H2","phosphate",1),
	Pyruvate("COCOCH3","pyruvate",1),
	Sulfate("SO3H","sulfate",1),
	Thio("SH","thio",1),
	RPyruvate("CH2CCOOH","(r)-pyruvate",2),
	SPyruvate("CH2CCOOH","(s)-pyruvate",2),
	RLactate2("CH3CHCO","(r)-lactate",2),
	SLactate2("CH3CHCO","(s)-lactate",2),
	Anhydro("-H2O from basetye (intramolecular ether)","anhydro",2),
	Lactone("-H2O from basetye (intramolecular ester)","lactone",2),
	Epoxy("-H20 from basetype (neighbouring C-atoms)","epoxy",2),
	
	Unknown("Unknown","unknown",0),
	
	Ceramide("Ceramide","ceramide",1),
	Alkylacylglycol("Alkylacylglycol","alkylacylglycol",1)
	;
	


	private String symbol;
    private String description;
    private int bondOrder;
    
	
    public String getSymbol() {
		return symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getBondOrder() {
		return bondOrder;
	}


	public void setBondOrder(int bondOrder) {
		this.bondOrder = bondOrder;
	}

    
    
    /** Private constructor, see the forName methods for external use. */
    private Substituent( String description, String symbol, int bondOrder ) 
    { 
        this.symbol = symbol; 
        this.description = description; 
        this.bondOrder = bondOrder;
    }
    
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static Substituent fromString(String symbol) {
        if (symbol != null) 
        {
          for (Substituent s : Substituent.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol)) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    


}
