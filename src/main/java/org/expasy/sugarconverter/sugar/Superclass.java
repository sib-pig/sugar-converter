package org.expasy.sugarconverter.sugar;

public enum Superclass 
{
	Tri("Triose","TRI",3),
	Tet("Tetrose","TET",4),
	Pen("Pentose","PEN",5),
	Hex("Hexose","HEX",6),
	Hep("Heptose","HEP",7),
	Oct("Octose","OCT",8),
	Non("Nonose","NON",9),
	Dec("Decose","DEC",10),
	S11("Undecose","S11",11),
	S12("Dodecose","S12",12),
	S13("Tridecose","S13",13),
	S14("Tetradecose","S14",14),
	S15("Pentadecose","S15",15),
	S16("Hexadecose","S16",16)
	;
	
	/** Stem name */
    private String fullname;
	/** Stem short name. */
    private String symbol;
    /** number of linear oriented C-atoms */
    private int nbC;
    
    /** Private constructor, see the forName methods for external use. */
    private Superclass( String fullname, String symbol, int nbC) 
    { 
        this.fullname = fullname; 
        this.symbol = symbol; 
        this.nbC = nbC;
    }
     
    
    /** Returns the appropriate Stem instance for the given symbol.  */
    public static Superclass fromString(String symbol) {
        if (symbol != null) 
        {
          for (Superclass s : Superclass.values()) 
          {
            if (symbol.equalsIgnoreCase(s.symbol)) 
            {
              return s;
            }
          }
        }
        return null;
      }
    
    public String getFullname() {
		return fullname;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public int getNbC() {
		return nbC;
	}
}