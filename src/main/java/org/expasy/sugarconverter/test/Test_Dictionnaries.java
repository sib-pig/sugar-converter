package org.expasy.sugarconverter.test;


import java.util.HashMap;
import java.util.Map;

import static org.expasy.sugarconverter.parser.Constants.*;

public class Test_Dictionnaries {

	public static Map getHexoses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("All", RES_HEADER + "1b:x-dall-HEX-1:5");
		glycans.put("Alt", RES_HEADER + "1b:x-dalt-HEX-1:5");
		glycans.put("Gal", RES_HEADER + "1b:x-dgal-HEX-1:5");
		glycans.put("Glc", RES_HEADER + "1b:x-dglc-HEX-1:5");
		glycans.put("Gul", RES_HEADER + "1b:x-dgul-HEX-1:5");
		glycans.put("Hex", RES_HEADER + "1b:x-HEX-1:5");
		glycans.put("Ido", RES_HEADER + "1b:x-lido-HEX-1:5");
		glycans.put("Man", RES_HEADER + "1b:x-dman-HEX-1:5");
		glycans.put("Tal", RES_HEADER + "1b:x-dtal-HEX-1:5");
		glycans.put("Galf", RES_HEADER + "1b:x-dgal-HEX-1:4");
		glycans.put("LAlt", RES_HEADER + "1b:x-lalt-HEX-1:5");
		glycans.put("LGal", RES_HEADER + "1b:x-lgal-HEX-1:5");
		glycans.put("LTal", RES_HEADER + "1b:x-ltal-HEX-1:5");
		glycans.put("LIdo", RES_HEADER + "1b:x-lido-HEX-1:5");

		return glycans;
	}

	public static Map getPentoses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Ara", RES_HEADER + "1b:x-dara-PEN-1:5");
		glycans.put("Araf", RES_HEADER + "1b:x-dara-PEN-1:4");
		glycans.put("LAra", RES_HEADER + "1b:x-lara-PEN-1:5");
		glycans.put("LAraf", RES_HEADER + "1b:x-lara-PEN-1:4");
		glycans.put("Lyx", RES_HEADER + "1b:x-dlyx-PEN-1:5");
		glycans.put("Pent", RES_HEADER + "1b:x-PEN-1:5");
		glycans.put("Pentf", RES_HEADER + "1b:x-PEN-1:4");
		glycans.put("Rib", RES_HEADER + "1b:x-drib-PEN-1:5");
		glycans.put("Ribf", RES_HEADER + "1b:x-drib-PEN-1:4");
		glycans.put("Xyl", RES_HEADER + "1b:x-dxyl-PEN-1:5");

		return glycans;
	}

	public static Map getTetroses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Ery", RES_HEADER + "1b:x-dery-TET-1:4");
		glycans.put("Thr", RES_HEADER + "1b:x-dthr-TET-1:4");

		return glycans;
	}

	public static Map getTrioses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Gro", RES_HEADER + "1b:x-xgro-TRI-X:X");

		return glycans;
	}

	public static Map getHeptoses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Dha", RES_HEADER + "1b:x-dlyx-HEP-2:6|1:a|2:keto|3:d|7:a");
		glycans.put("GroMan", RES_HEADER + "1b:x-dgro-dman-HEP-1:5|1:a|2:keto|3:d|7:a");
		glycans.put("Hep", RES_HEADER + "1b:x-HEP-1:5");
		glycans.put("LGroMan", RES_HEADER + "1b:x-lgro-dman-HEP-1:5|1:a|2:keto|3:d|7:a");

		return glycans;
	}

	public static Map getKetoses(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Fru", RES_HEADER + "1b:x-dara-HEX-2:6|2:keto");
		glycans.put("LFru", RES_HEADER + "1b:x-lara-HEX-2:6|2:keto");
		glycans.put("Fruf", RES_HEADER + "1b:x-dara-HEX-2:5|2:keto");
		glycans.put("LSor", RES_HEADER + "1b:x-lxyl-HEX-2:6|2:keto");
		glycans.put("Sed", RES_HEADER + "1b:x-dalt-HEP-2:6|2:keto");
		glycans.put("Sor", RES_HEADER + "1b:x-dxyl-HEX-2:6|2:keto");
		glycans.put("Tag", RES_HEADER + "1b:x-dlyx-HEX-2:6|2:keto");
		glycans.put("Xul", RES_HEADER + "1b:x-dthr-PEN-2:5|2:keto");

		return glycans;
	}

	public static Map getUronicAcids(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("AllA", RES_HEADER + "1b:x-dall-HEX-1:5|6:a");
		glycans.put("GalA", RES_HEADER + "1b:x-dgal-HEX-1:5|6:a");
		glycans.put("GlcA", RES_HEADER + "1b:x-dglc-HEX-1:5|6:a");
		glycans.put("GroA2", RES_HEADER + "1b:x-xgro-TRI-X:X|0:a|0:a");
		glycans.put("GulA", RES_HEADER + "1b:x-dgul-HEX-1:5|6:a");
		glycans.put("HexA", RES_HEADER + "1b:x-HEX-1:5|6:a");
		glycans.put("IdoA", RES_HEADER + "1b:x-lido-HEX-1:5|6:a");
		glycans.put("LAltA", RES_HEADER + "1b:x-lalt-HEX-1:5|6:a");
		glycans.put("LGulA", RES_HEADER + "1b:x-lgul-HEX-1:5|6:a");
		glycans.put("ManA", RES_HEADER + "1b:x-dman-HEX-1:5|6:a");
		glycans.put("TalA", RES_HEADER + "1b:x-dtal-HEX-1:5|6:a");
		//deoxy
		glycans.put("DeoxyGalA", RES_HEADER + "1b:x-dgal-HEX-1:5|6:a|0:d");
		glycans.put("DeoxyGlcA", RES_HEADER + "1b:x-dglc-HEX-1:5|6:a|0:d");
		glycans.put("DeoxyIdoA", RES_HEADER + "1b:x-lido-HEX-1:5|6:a|0:d");

		return glycans;
	}

	public static Map getDeoxys(){

		Map<String, String> glycans = new HashMap<String, String>();;

		//Mono
		glycans.put("deoxyGlc", RES_HEADER + "1b:x-dglc-HEX-1:5|6:d");
		glycans.put("deoxyHex", RES_HEADER + "1b:x-HEX-1:5|6:d");
		glycans.put("deoxyTal", RES_HEADER + "1b:x-dtal-HEX-1:5|6:d");
		glycans.put("DFuc", RES_HEADER + "1b:x-dgal-HEX-1:5|6:d");
		glycans.put("deoxyGal", RES_HEADER + "1b:x-dgal-HEX-1:5|6:d");
		glycans.put("DRha", RES_HEADER + "1b:x-dman-HEX-1:5|6:d");
		glycans.put("dAlt", RES_HEADER + "1b:x-lalt-HEX-1:5|6:d");
		glycans.put("Fuc", RES_HEADER + "1b:x-lgal-HEX-1:5|6:d");
		glycans.put("LFuc", RES_HEADER + "1b:x-lgal-HEX-1:5|6:d");
		glycans.put("Rha", RES_HEADER + "1b:x-lman-HEX-1:5|6:d");
		glycans.put("LRha", RES_HEADER + "1b:x-lman-HEX-1:5|6:d");
		glycans.put("Qui", RES_HEADER + "1b:x-dglc-HEX-1:5|6:d");

		//Di
		glycans.put("Abe", RES_HEADER + "1b:x-dxyl-PEN-1:5|3:d|6:d");
		glycans.put("Asc", RES_HEADER + "1b:x-lara-PEN-1:5|3:d|6:d");
		glycans.put("Col", RES_HEADER + "1b:x-lxyl-PEN-1:5");
		glycans.put("Oli", RES_HEADER + "1b:x-dara-PEN-1:5|2:d|6:d");
		glycans.put("Tyv", RES_HEADER + "1b:x-dara-PEN-1:5|3:d|6:d");

		//Tri
		glycans.put("Bac", RES_HEADER + "1b:x-lglc-HEX-1:5|2:d|4:d|6:d");
		glycans.put("Ami", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|2:d|3:d|6:d");

		return glycans;
	}

	public static Map getKetoUlusonicAcids(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("Kdn", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d");
		glycans.put("Kdo", RES_HEADER + "1b:x-dman-OCT-2:6|1:a|2:keto|3:d");
		glycans.put("Ko", RES_HEADER + "1b:x-dgro-dtal-OCT-2:6|1:a|2:keto");

		return glycans;
	}

	public static Map getAlcohols(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("GalOl", RES_HEADER + "1b:x-dgal-HEX-0:0|1:aldi");
		glycans.put("GlcOl", RES_HEADER + "1b:x-dglc-HEX-0:0|1:aldi");
		glycans.put("HexOl", RES_HEADER + "1b:x-HEX-0:0|1:aldi");


		return glycans;
	}


	public static Map getAcetamidos(){

		Map<String, String> glycans = new HashMap<String, String>();;

//		glycans.put("GalOl", RES_HEADER + "1b:x-dgal-HEX-0:0|1:aldi");
		glycans.put("GalNAcol", RES_HEADER + "1b:o-dgal-HEX-0:0|1:aldi\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GlcNAcol", RES_HEADER + "1b:o-dglc-HEX-0:0|1:aldi\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("HexNAcol", RES_HEADER + "1b:o-HEX-0:0|1:aldi\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");

		glycans.put("deoxyGlcNAc", RES_HEADER + "1b:x-dglc-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("deoxyGalNAc", RES_HEADER + "1b:x-dgal-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");

		glycans.put("GalNAc", RES_HEADER + "1b:x-dgal-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GlcNAc", RES_HEADER + "1b:x-dglc-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("AllNAc", RES_HEADER + "1b:x-dall-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GulNAc", RES_HEADER + "1b:x-dgul-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("LAltNAc", RES_HEADER + "1b:x-lalt-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("LIdoNAc", RES_HEADER + "1b:x-lido-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("TalNAc", RES_HEADER + "1b:x-dtal-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("ManNAc", RES_HEADER + "1b:x-dman-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("HexNAc", RES_HEADER + "1b:x-HEX-1:5\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");

		glycans.put("LFucNAc", RES_HEADER + "1b:x-lgal-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("FucNAc", RES_HEADER + "1b:x-lgal-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("DFucNAc", RES_HEADER + "1b:x-dgal-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("LRhaNAc", RES_HEADER + "1b:x-lman-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("QuiNAc", RES_HEADER + "1b:x-dglc-HEX-1:5|6:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n");

		glycans.put("BacAc2", RES_HEADER + "1b:x-lglc-HEX-1:5|2:d|4:d|6:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "LIN\n" + "1:1d(2+1)2n\n" + "2:1d(4+1)3n");
		glycans.put("GlcNGc", RES_HEADER + "1b:x-dglc-HEX-1:5\n" + "2s:n-glycolyl\n" + "LIN\n" + "1:1d(2+1)2n");

		return glycans;
	}

	public static Map getAminos(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("deoxyGalN", RES_HEADER + "1b:x-dgal-HEX-1:5|6:d\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("deoxyGlcN", RES_HEADER + "1b:x-dglc-HEX-1:5|6:d\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GalN", RES_HEADER + "1b:x-dgal-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GlcN", RES_HEADER + "1b:x-dglc-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("AllN", RES_HEADER + "1b:x-dall-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GulN", RES_HEADER + "1b:x-dgul-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("LAltN", RES_HEADER + "1b:x-lalt-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("LIdoN", RES_HEADER + "1b:x-lido-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("TalN", RES_HEADER + "1b:x-dtal-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("HexN", RES_HEADER + "1b:x-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("ManN", RES_HEADER + "1b:x-dman-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(2+1)2n");

		glycans.put("GlcNS", RES_HEADER + "1b:x-dglc-HEX-1:5\n" + "2s:n-sulfate\n" + "LIN\n" + "1:1d(2+1)2n");
		glycans.put("GlcNH2", RES_HEADER + "1b:x-dglc-HEX-1:5\n" + "2s:amino\n" + "LIN\n" + "1:1d(3+1)2n");

		return glycans;
	}

	public static Map getSialics(){

		Map<String, String> glycans = new HashMap<String, String>();;

		glycans.put("NeuAc2", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "LIN\n" + "1:1d(-1+1)2n\n" + "2:1d(-1+1)3n");
		glycans.put("Neu4,5Ac2", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "LIN\n" + "1:1d(4+1)2n\n" + "2:1d(5+1)3n");
		glycans.put("Neu5,9Ac2", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "LIN\n" + "1:1d(5+1)2n\n" + "2:1d(9+1)3n");
		glycans.put("Neu5,?Ac2", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "LIN\n" + "1:1d(-1+1)2n\n" + "2:1d(5+1)3n");
		glycans.put("Neu5,?,?Ac3", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "3s:n-acetyl\n" + "4s:n-acetyl\n" + "LIN\n" + "1:1d(-1+1)2n\n" + "2:1d(-1+1)3n\n" + "3:1d(5+1)4n");
		glycans.put("NeuAc", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(5+1)2n");
		glycans.put("Neu5Ac", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(5+1)2n");
		glycans.put("NeuGc", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-glycolyl\n" + "LIN\n" + "1:1d(5+1)2n");
		glycans.put("Neu?c", RES_HEADER + "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" + "2s:n-acetyl\n" + "LIN\n" + "1:1d(5+1)2n");

		return glycans;
	}











}
