package org.expasy.sugarconverter.test;

import java.util.Map;
import java.util.Set;

import static org.expasy.sugarconverter.parser.Constants.*;

/**
 * Created by julien on 09.03.17.
 */
public class Test_Generator {

    public String generateMnonosaccharideTests(Map<String,String> monosaccharides){
        String tests = "";
        Set<String> keys = monosaccharides.keySet();
        for(String key : keys){
            tests += generateMnonosaccharideTest(key) + EOL;
        }

        System.out.println(tests);
        return tests;
    }

    private String generateMnonosaccharideTest(String monosaccharide) {

        String test = "        @Test" + EOL +
                "        public void getCtSequence_" + monosaccharide + "() {" + EOL +
                "            testCtSequence(\"" + monosaccharide + "\");" + EOL +
                "        }";
        return test;
    }



}
