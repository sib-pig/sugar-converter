package org.expasy.sugarconverter.test;

import org.expasy.sugarconverter.parser.IupacParser;

import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by julien on 09.03.17.
 */
public class Test_Common {

    public static void testCtSequenceDict(String iupac, Map dict) {

        String glycoct = getGlycoCtSequence(iupac);

        System.out.println("=======================");
        System.out.println("iupac : "+ iupac);
        System.out.println("glycoct convert : "+ glycoct);
        System.out.println("glycoct ref : "+ dict.get(iupac));


        assertNotNull(glycoct);
        assertFalse(glycoct.equals(""));
        assertTrue(glycoct.equals((String) dict.get(iupac)));
    }

    public static String getGlycoCtSequence(String iupac) {

        IupacParser p= new IupacParser(iupac);
        String output=null;

        try{
            p.getCtTree(p.parse());
            output = p.getCtSequence();
        }catch(Exception ex){
            System.err.println("Problem parsing the sequence" + ex.getMessage());
        }
        return output;
    }



}
