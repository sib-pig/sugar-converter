package org.expasy.sugarconverter.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ResultFile {

	File file = null;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public ResultFile(String fileName)
	{
		file = new File(fileName);
		if(file.exists())
		{
			file.delete();
		}
	}
	
	public void writeToFile(String text) 
	{
        try 
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter(this.file,true));
            bw.write(text);
            bw.newLine();
            bw.close();
        } 
        catch (Exception ex) 
	    {
        	System.err.println(ex.getMessage());
	    }
	}
	
	
	
	
}
