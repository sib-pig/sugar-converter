package org.expasy.sugarconverter.exec;

import org.expasy.sugarconverter.parser.IupacParser;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	String input1 = "Glc(a1-4)Gal";//"D-Glc(a1-4)D-Gal";
	String input2 = "Ara(?1-?)[GlcNAc(b1-?)]Man(a1-?)[Man(a1-?)][Xyl(?1-?)]Man(b1-4)Glc(b1-4)[Fuc(?1-?)]GlcNAc";
	String input3 = "Gal(b1-4)GlcNAc(b1-4)[GlcNAc(b1-2)]Man(a1-3)[GlcNAc(b1-2)[GlcNAc(b1-4)][GlcNAc(b1-6)]Man(a1-6)][GlcNAc(b1-4)]Man(b1-4)GlcNAc(b1-4)GlcNAc";
	String input4 = "Glc";
	String input10 = "NeuAc";
	String input11 = "Glc( 1-6)[GlcN( 1-3)]Glc( 1-6)Glc( 1-6)[Glc( 1-3)]Glc";
	String input12 = "NeuAc(a2-3)Gal(b1-4)GlcNAc(b1-2)[NeuAc(a2-3)Gal(b1-4)GlcNAc(b1-?)]Man(a1-?)[NeuAc(a2-3)Gal(b1-4)GlcNAc(b1-2)Man(a1-?)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc";
	String input13 = "NeuAc(a2-3)Gal(b1-3)[HSO3(-6)]GalNAc";
	String input14 = "HSO3(-?)GlcNAc(?1-6)GalNAc";
	
	//Galf
	String input15 = "Galf(a1-2)Gal";
	//ManNAc
	String input16 = "Gal(b1-4)ManNAc"; 
	//GlcA
	String input17 = "GlcA(a1-6)Man"; 
	//Kdn
	String input18 = "Kdn(a2-3)[Kdn(a2-6)]GalNAc";
	//BacAc2
	String input8 = "Gal(b1-4)Gal(a1-3){2,4-diacetamido-2,4,6-trideoxyhexose}";
	

	//P link
	String input9 = "Glc(b1-3)[Man(a1-P-6)]Gal(b1-4)Man(a1-P-6)[Glc(b1-3)]Gal(b1-4)Man(a1-P)";
	String input19 = "Gal(b1-4)Man(a1-P)";
	String input20 = "Man(a1-P-6)Gal(b1-4)Man";
	String input21 = "Man(a1-2)[EtN(-P-6)]Man";
	String input22 = "Man(a1-2)[Gal(-P-6)]Man";
	
	String input23 = "Pent(?1-?)Pent";
	String input24 = "Neu?c(a2-6)Gal(b1-4)[HSO3(-6)]GlcNAc(b1-2)Man(a1-3)[Neu?c(a2-6)Gal(b1-4)GlcNAc(b1-2)Man(a1-6)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc";
	String input25 = "Neu4,5Ac2(a2-6)Gal(b1-4)GlcNAc(b1-2)Man(a1-6)[NeuAc(a2-6)Gal(b1-4)GlcNAc(b1-2)Man(a1-3)]Man(b1-4)GlcNAc(b1-4)GlcNAc";
	String input26 = "Neu4,5Ac2(a2-3)Gal(b1-4)Glc";
	String input27 = "Neu4,5Ac2";
	String input28 = "NeuAc";
	
	String input29 = "Fuc(?1-?)Gal(?1-?)[Fuc(?1-?)]GalNAc(?1-?)Gal(?1-?)GlcNAc(?1-?)[NeuAc(?2-?)Gal(?1-?)]GalNAc";

	String input30 = "Me(-3)Rha(a1-4)Hex(a1-4)Man(a1-3)Rha(a1-3)Rha(a1-3)Rha(a1-3)Gal";
	
	String input31 = "Gal(b1-4)GlcNAc(b1-3)Gal(b1-4)Glc(b1-1)P(a1-2)Cer";
	
	String input32 = "Ara(?1-?)[GlcNAc(b1-?)]Man(a1-?)[Man(a1-?)][Xyl(?1-?)]Man(b1-4)GlcNAc(b1-4)[Fuc(?1-?)]Man(a1-?)GlcNAc";
	
	
	//Repeats
	String input5 = "Gal(?1-?)GlcNAc(?1-?)[Gal(?1-?)GlcNAc(?1-?)]Man(a1-3)[Gal(?1-?)GlcNAc(?1-?)[Gal(?1-?)GlcNAc(?1-?)]Man(a1-6)][GlcNAc(?1-4)]Man(b1-4)GlcNAc(b1-4)[Fuc(?1-6)]GlcNAc+\"+ 3 x Gal(?1-?)GlcNAc(?1-?) + Fuc(?1-?)\"";
	String input36 = "Gal(?1-?)GlcNAc(?1-?)[Gal(?1-?)GlcNAc(?1-?)]Man(a1-3)[Gal(?1-?)GlcNAc(?1-?)[Gal(?1-?)GlcNAc(?1-?)]Man(a1-6)][GlcNAc(?1-4)]Man(b1-4)GlcNAc(b1-4)[Fuc(?1-6)]GlcNAc{Gal(?1-?)GlcNAc(?1-?)Fuc(?1-?)}3";
	String input6 = "Me(-3)Rha(a1-4){Man(b1-3)Rha(a1-4)}26Man(a1-3)Rha(a1-3)Rha(a1-3)Rha(a1-3)Gal";
	String input33 = "Me(-3)Rha(a1-4){Man(b1-3)Rha(a1-4)}26Man(a1-3){Rha(a1-3)Rha(a1-3)Rha(a1-3)}12Gal";
	String input34 = "{Man(b1-3)Rha(a1-4)}26";
	//FIXME:index of nested repeats
	String input35 = "{{Man(b1-3)}17-19Rha(a1-4)}26";

	//Repeats double quotes
	String input7 = "Gal(b1-4)[Glc(a1-6)]ManNAc(b1-3){Gal(b1-4)[Glc(a1-6)]ManNAc}j(b1-3)Gal(b1-4)[Glc(a1-6)]ManNAc(a1-3)[GroA2(u?-?)P(u?-4)Man(b1-4)]Rha(a1-3)Rha(a1-3)Rha(a1-3)Gal+\"where j = 20\"";
		
	//Comments
	String input37 = "Man(b1-4)GlcNAc(b1-4)GlcNAc+\"+ 30 x Man\"";//ok
	String input38 = "NeuAc(?2-?)Gal(b1-3)GalNAc+\"+  NeuAc(?2-?)\""; //ok
	String input39 = "NeuAc(?2-?)Gal(b1-3)GalNAc+\"+ 9 x NeuAc(?2-?)\"";
	String input40 = "Man(b1-4)GlcNAc(b1-4)GlcNAc+\"+ Man\"";
	String input41 = "Gal(?1-?)GalNAc+\"+ NeuAc + NeuGc\"";
	
	String input42 = "Hex(?1-?)Hex(?1-?)deoxyHex(?1-?)Hex(?1-?)HexNAc";
	String input43 = "Hex(?1-?)HexNAc(?1-?)[Hex(?1-?)HexNAc(?1-?)]Man(a1-?)[Hex(?1-?)HexNAc(?1-?)[HexNAc(?1-?)HexNAc(?1-?)]Man(a1-?)]Man(b1-4)GlcNAc(b1-4)[Fuc(?1-?)]GlcNAc+\"+ (3 x NeuAc)\"";
	
	String input ="NeuAc(a2-?)Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}1-13GlcNAc(b1-2)Man(a1-?)[Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}1-13GlcNAc(b1-2)Man(a1-?)][GlcNAc(b1-4)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc" ;
	
	String input44 ="Hex";
	String input45 ="Neu5,?,?Ac3(a2-3)Gal(b1-4)[HSO3(-6)]GlcNAc(b";
	
	String input46 = "Neu5,9Ac2(a2-3)Gal(b1-4)GlcNAc(b1-2)[Neu5,9Ac2(a2-3)Gal(b1-4)GlcNAc(b1-4)]Man(a1-3)[Neu5,9Ac2(a2-3)Gal(b1-4)GlcNAc(b1-2)[NeuAc2(a2-3)Gal(b1-4)GlcNAc(b1-6)]Man(a1-6)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc";
	String input47 = "NeuAc(a2-?)Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}1-13GlcNAc(b1-2)Man(a1-3)[NeuAc(a2-?)Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}1-13GlcNAc(b1-2)Man(a1-6)]Man(b1-4)GlcNAc(b1-4)GlcNAc";
	
	String input48 ="Rha";
	String input49 ="Neu5,?,?Ac3";
	
	String input50 = "Man(b1-4)NeuAc(a2-3)Gal";
	String input51 = "Man(b1-4)[Glc(a2-3)]Gal";
	String input52 = "Man(b1-4)Neu5,9Ac2(a2-3)Gal";
	String input53 = "Man(b1-4)Neu5,9Ac2(a2-3)Gal";	
	
	String input54 = "NeuGc(a2-8)NeuGc(a2-6)GalNAc";
	String input55 = "Gal(?1-?)GlcNAc";
	
	String input56 = "Man(a1-2)Man(a1-3)[Man(a1-3)]Man(a1-6)[Man(a1-6)]Man";
//	String input57 = "Gal(b1-3)GalNAc(b1-4)[NeuAc(a2-3)]Gal(b1-4)Glc(b1-1)Cer";
	String input57 = "NeuAc(a2-3)[Gal(b1-3)GalNAc(b1-4)]Gal(b1-4)Glc(b1-1)Cer";
	
	//UND double quotes
	String input58 = "Gal(b1-?)GalNAc+\"+ 2 x NeuAc\"";
	String input59 = "Gal(b1-3)GalNAc(a3-1)+\" + Fuc(a3-8)Man + 2 x Man(a1-2)NeuAc(a3-5)Glc(a4-9)\"";
		
	String input60 = "NeuAc(a2-6)Gal(b1-4)[Su-6]GlcNAc(b1-?)";
	String input61 = "Gal(b1-4)[HSO3(-6)]GlcNAc(b1-2)Man(a1-3)[Man(a1-6)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc+\"+ 2xHex(a1-?)\"";
	
	String input62 = "Me(-3)Rha(a1-4){Man(b1-3)Rha(a1-4)}26Man(a1-3)Rha(a1-3)Rha(a1-3)Rha(a1-3)Gal";
	String input63 = "Gal(b1-4)GlcNAc(b1-?)[GlcNAc(b1-?)]Man(a1-?)[GlcNAc(b1-?)Man(a1-?)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc+\"+ GalNac(b1-4) + NeuAc(a2-3) + NeuAc(a2-3)Gal(b1-4)\"";
	
	String input64 = "Gal(b1-4)[Fuc(a1-3)]GlcNAc(b1-2)Man(a1-6)";
	String input65 = "Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}13GlcNAc(b1-2)Man(a1-3)[Gal(b1-4){GlcNAc(b1-3)Gal(b1-4)}13GlcNAc(b1-2)Man(a1-6)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc";
	
	String input66 = "GalNAc(b1-4)Gal(b1-4)Glc(b1-1)Cer";//"Fuc(a1-2)Gal(b1-3)[Fuc(a1-4)]GlcNAc(b1-3)Gal";//"Glc(b1-4)[Man(b1-3)]Gal(b1-?)";
	
	String input70 = "SO4(-3)Gal(b1-1)";
	String input71 = "SO4(-3)Gal(b1-3)alkylacylglycol";
	String input72 = "SO4(-3)Gal(b1-4)Glc(b1-1)";
	String input73 = "SO4(-6)Gal(b1-1)";
	String input74 = "SO3Gal(b1-1)";
	String input75 = "SO3Gal(b1-3)GalNAc(b1-4)Gal(b1-4)Glc(b1-1)";
	String input76 = "NeuAc(a2-3)(2F)Gal(b1-3)GlcNAc";
	String input77 = "NeuAc(a2-3)(2F)Gal(b1-4)GlcNAc";
	String input78 = "Gal(b1-3)GalNAc(b1-4)[SU-3]Gal(b1-4)Glc(b1-1)";

//    String input99 = "P-6-Man(a1-2)Man(a1-3)[Man(a1-3)][P-6-Man(a1-6)]Man(a1-6)Man(b1-4)GlcNAc(b?- ?)";
    String input99 = "P-6-Man(a1-2)Man(a1-3)[Man(a1-3)][P-6-Man(a1-6)]Man(a1-6)Man(b1-4)GlcNAc(b?- ?)";
//    String input9 = "Glc(b1-3)[Man(a1-P-6)]Gal(b1-4)Man(a1-P-6)[Glc(b1-3)]Gal(b1-4)Man(a1-P)";

    String input100 = "[Su-2][Su-3][Su-4][Su-6]Glc(a1-4)[Su-2][Su-3][Su-6]Glc(a1-4)[Su-2][Su-3][Su-6]Glc(b1-4)[Su-6]Glc(a1-4)Glc(b1-4)Glc(a1-4)Glc(b1-4)Glc(a1-4)Glc(b1-4)Glc(a1-4)Glc(b1-4)[Su-6]Glc(b1-4)[Su-2][Su-3][Su-6]Glc(a1-4)IdoA(a1-4)Glc";
    String input101 = "Man(a1-2)Man(a1-6)Man(a1-4)GlcNH2(a1-6)";
    String input102 = "P(1-6)Man(a1-3)Man(a1-3)Man(a?-?)";
    String input103 = "Gal(b1-3)[GlcNAc(b1-6)]GalNAcol";
    String input104 = "HexNAc(??-?)HexNAc(??-?)Gal(b1-4)GlcNAc(b1-6)[Gal(b1-4)GlcNAc(b1-6)[Gal(b1-4)GlcNAc(b1-3)]Gal(b1-3)]GalNAc";

    //GlcNS
    String input105 = "GlcA(b1-4)GlcNS";

    //Pyruvate
	String input106 = "Pyr(1-?)Man";

    //Underdetermined linkage
    String input107 = "GalNAc(a1-3)[Fuc(a1-2)]Gal(b1-3/4)GlcNAc(b1-3/6)[GalNAc(a1-3)[Fuc(a1-2)]Gal(b1-3/4)[Fuc(a1-3/4)]GlcNAc(b1-3/6)]Gal(b1-3/4)GlcNAc(b1-3)Gal(b1-3)[Gal(b1-4)GlcNAc(b1-6)]GalNAc";



        long begin = System.nanoTime();
	IupacParser p= new IupacParser(input107);
	p.setGlycanType("-LINKED"); //N-LINKED (-> beta), O-LINKED (-> alpha)
	
	
	try
	{
		
		p.getCtTree(p.parse());
		System.out.println("IUPAC sequence : ");
		System.out.println(p.getIupacSequence());
		System.out.println("");
		System.out.println("CT sequence : ");
		System.out.println(p.getCtSequence());
		
	}
	catch(Exception ex)
	{
		System.err.println("Problem parsing the sequence" + ex.getMessage());
		ex.printStackTrace();
	}
	long end = System.nanoTime();
	
	System.out.println("");
	System.out.println("Process time [s] : " + (end-begin)*Math.pow(10, -9));
	
	
//	System.out.println("hasComment : " + TreeTools.hasCommentChar(p.getIupacSequence())) ;
//	System.out.println("hasRepeat : " + TreeTools.hasRepeatChar(p.getIupacSequence())) ;
	
//System.out.println("Modifier.Keto : " + Modifier.fromString("ket").getSymbol() + '\t' + Modifier.fromString("ket").getDescription());
//	Ara a = new Ara();
//	a.toString();

	}
	
	

}
