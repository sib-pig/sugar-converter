package org.expasy.sugarconverter.exec;

import org.expasy.sugarconverter.stats.ResidueStats;
import org.expasy.sugarconverter.database.DbNames;

public class StatsResidues {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		long begin = System.nanoTime();
		
		ResidueStats rstats = new ResidueStats();
//		rstats.getStatFile(DbNames.glycosuite,2);
//		rstats.getStatFile(DbNames.glycosuite,1);
//		rstats.getStatFile(DbNames.sugarbind,1);
//		rstats.getStatFile(DbNames.sugarbind12,1);
//		rstats.getStatFile(DbNames.gssb,1);
//		rstats.getStatFile(DbNames.gssb12,1);
		rstats.getStatFile(DbNames.gssb12,2);
		
		long end = System.nanoTime();
		
		System.out.println("");
		System.out.println("Total process time [s] : " + (end-begin)*Math.pow(10, -9));
	}
	
}
