package org.expasy.sugarconverter.exec;

import org.expasy.sugarconverter.test.*;

/**
 * Created by julien on 09.03.17.
 */
public class GenerateMonosaccharideTestMethods {
    /**
     * @param args
     */
    public static void main(String[] args) {
        Test_Generator generator = new Test_Generator();

//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getHexoses());
        generator.generateMnonosaccharideTests(Test_Dictionnaries.getPentoses());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getTrioses());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getTetroses());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getHeptoses());
//
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getKetoUlusonicAcids());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getAlcohols());
//
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getDeoxys());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getKetoses());
//        generator.generateMnonosaccharideTests(Test_Dictionnaries.getUronicAcids());



    }
}
