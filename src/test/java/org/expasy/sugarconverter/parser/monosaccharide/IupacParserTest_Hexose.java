package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Hexose {

    Map<String, String> hexoses = Test_Dictionnaries.getHexoses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all hexoses");
        System.out.println(Test_Dictionnaries.getHexoses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, hexoses);
    }

    @Test
    public void getCtSequence_All() {
        testCtSequence("All");
    }

    @Test
    public void getCtSequence_Alt() {
        testCtSequence("Alt");
    }

    @Test
    public void getCtSequence_Gal() {
        testCtSequence("Gal");
    }

    @Test
    public void getCtSequence_Galf() {
        testCtSequence("Galf");
    }

    @Test
    public void getCtSequence_Glc() {
        testCtSequence("Glc");
    }

    @Test
    public void getCtSequence_Gul() {
        testCtSequence("Gul");
    }

    @Test
    public void getCtSequence_Hex() {
        testCtSequence("Hex");
    }

    @Test
    public void getCtSequence_Ido() {
        testCtSequence("Ido");
    }

    @Test
    public void getCtSequence_LAlt() {
        testCtSequence("LAlt");
    }

    @Test
    public void getCtSequence_LGal() {
        testCtSequence("LGal");
    }

    @Test
    public void getCtSequence_Man() {
        testCtSequence("Man");
    }

    @Test
    public void getCtSequence_Tal() {
        testCtSequence("Tal");
    }

    @Test
    public void getCtSequence_LTal() {
        testCtSequence("LTal");
    }

    @Test
    public void getCtSequence_LIdo() {
        testCtSequence("LIdo");
    }


}