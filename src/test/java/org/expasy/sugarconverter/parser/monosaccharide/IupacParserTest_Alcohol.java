package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Alcohol {

    Map<String, String> alcohols = Test_Dictionnaries.getAlcohols();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all alcohols");
        System.out.println(Test_Dictionnaries.getAlcohols().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, alcohols);
    }

    @Test
    public void getCtSequence_GlcOl() {
        testCtSequence("GlcOl");
    }
    @Test
    public void getCtSequence_GalOl() {
        testCtSequence("GalOl");
    }
    @Test
    public void getCtSequence_HexOl() {
        testCtSequence("HexOl");
    }


}