package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Tetrose {

    Map<String, String> tetroses = Test_Dictionnaries.getTetroses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all tetroses");
        System.out.println(Test_Dictionnaries.getTetroses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, tetroses);
    }


    @Test
    public void getCtSequence_Ery() {
        testCtSequence("Ery");
    }
    @Test
    public void getCtSequence_Thr() {
        testCtSequence("Thr");
    }

}