package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Pentose {

    Map<String, String> pentoses = Test_Dictionnaries.getPentoses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all pentoses");
        System.out.println(Test_Dictionnaries.getPentoses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, pentoses);
    }

    @Test
    public void getCtSequence_Ara() {
        testCtSequence("Ara");
    }
    @Test
    public void getCtSequence_LAra() {
        testCtSequence("LAra");
    }
    @Test
    public void getCtSequence_Araf() {
        testCtSequence("Araf");
    }
    @Test
    public void getCtSequence_Lyx() {
        testCtSequence("Lyx");
    }
    @Test
    public void getCtSequence_Rib() {
        testCtSequence("Rib");
    }
    @Test
    public void getCtSequence_Xyl() {
        testCtSequence("Xyl");
    }
    @Test
    public void getCtSequence_LAraf() {
        testCtSequence("LAraf");
    }
    @Test
    public void getCtSequence_Ribf() {
        testCtSequence("Ribf");
    }
    @Test
    public void getCtSequence_Pent() {
        testCtSequence("Pent");
    }
    @Test
    public void getCtSequence_Pentf() {
        testCtSequence("Pentf");
    }



}