package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Ketose {

    Map<String, String> ketoses = Test_Dictionnaries.getKetoses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all ketoses");
        System.out.println(Test_Dictionnaries.getKetoses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, ketoses);
    }


    @Test
    public void getCtSequence_Sed() {
        testCtSequence("Sed");
    }
    @Test
    public void getCtSequence_Sor() {
        testCtSequence("Sor");
    }
    @Test
    public void getCtSequence_Fru() {
        testCtSequence("Fru");
    }
    @Test
    public void getCtSequence_LFru() {
        testCtSequence("LFru");
    }
    @Test
    public void getCtSequence_LSor() {
        testCtSequence("LSor");
    }
    @Test
    public void getCtSequence_Tag() {
        testCtSequence("Tag");
    }
    @Test
    public void getCtSequence_Fruf() {
        testCtSequence("Fruf");
    }
    @Test
    public void getCtSequence_Xul() {
        testCtSequence("Xul");
    }

}