package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_KetoUlusonicAcid {

    Map<String, String> ketoUlusonicAcids = Test_Dictionnaries.getKetoUlusonicAcids();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all ketoUlusonicAcids");
        System.out.println(Test_Dictionnaries.getKetoUlusonicAcids().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, ketoUlusonicAcids);
    }

    @Test
    public void getCtSequence_Kdn() {
        testCtSequence("Kdn");
    }
    @Test
    public void getCtSequence_Ko() {
        testCtSequence("Ko");
    }
    @Test
    public void getCtSequence_Kdo() {
        testCtSequence("Kdo");
    }


}