package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Deoxy {

    Map<String, String> deoxys = Test_Dictionnaries.getDeoxys();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all deoxys");
        System.out.println(Test_Dictionnaries.getDeoxys().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, deoxys);
    }

    @Test
    public void getCtSequence_Col() {
        testCtSequence("Col");
    }
    @Test
    public void getCtSequence_deoxyGal() {
        testCtSequence("deoxyGal");
    }
    @Test
    public void getCtSequence_Qui() {
        testCtSequence("Qui");
    }
    @Test
    public void getCtSequence_Abe() {
        testCtSequence("Abe");
    }
    @Test
    public void getCtSequence_Bac() {
        testCtSequence("Bac");
    }
    @Test
    public void getCtSequence_DRha() {
        testCtSequence("DRha");
    }
    @Test
    public void getCtSequence_deoxyGlc() {
        testCtSequence("deoxyGlc");
    }
    @Test
    public void getCtSequence_Rha() {
        testCtSequence("Rha");
    }
    @Test
    public void getCtSequence_deoxyHex() {
        testCtSequence("deoxyHex");
    }
    @Test
    public void getCtSequence_dAlt() {
        testCtSequence("dAlt");
    }
    @Test
    public void getCtSequence_LFuc() {
        testCtSequence("LFuc");
    }
    @Test
    public void getCtSequence_Oli() {
        testCtSequence("Oli");
    }
    @Test
    public void getCtSequence_DFuc() {
        testCtSequence("DFuc");
    }
    @Test
    public void getCtSequence_Asc() {
        testCtSequence("Asc");
    }
    @Test
    public void getCtSequence_Tyv() {
        testCtSequence("Tyv");
    }
    @Test
    public void getCtSequence_Fuc() {
        testCtSequence("Fuc");
    }
    @Test
    public void getCtSequence_deoxyTal() {
        testCtSequence("deoxyTal");
    }
    @Test
    public void getCtSequence_LRha() {
        testCtSequence("LRha");
    }
    @Test
    public void getCtSequence_Ami() {
        testCtSequence("Ami");
    }


}