package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Heptose {

    Map<String, String> heptoses = Test_Dictionnaries.getHeptoses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all heptoses");
        System.out.println(Test_Dictionnaries.getHeptoses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, heptoses);
    }


    @Test
    public void getCtSequence_Hep() {
        testCtSequence("Hep");
    }
    @Test
    public void getCtSequence_GroMan() {
        testCtSequence("GroMan");
    }
    @Test
    public void getCtSequence_LGroMan() {
        testCtSequence("LGroMan");
    }
    @Test
    public void getCtSequence_Dha() {
        testCtSequence("Dha");
    }



}