package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Triose {

    Map<String, String> trioses = Test_Dictionnaries.getTrioses();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all trioses");
        System.out.println(Test_Dictionnaries.getTrioses().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, trioses);
    }

    @Test
    public void getCtSequence_Gro() {
        testCtSequence("Gro");
    }


}