package org.expasy.sugarconverter.parser.monosaccharide;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_UronicAcid {

    Map<String, String> uronicAcids = Test_Dictionnaries.getUronicAcids();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all uronicAcids");
        System.out.println(Test_Dictionnaries.getUronicAcids().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, uronicAcids);
    }

    @Test
    public void getCtSequence_HexA() {
        testCtSequence("HexA");
    }
    @Test
    public void getCtSequence_GulA() {
        testCtSequence("GulA");
    }
    @Test
    public void getCtSequence_GlcA() {
        testCtSequence("GlcA");
    }
    @Test
    public void getCtSequence_ManA() {
        testCtSequence("ManA");
    }
    @Test
    public void getCtSequence_TalA() {
        testCtSequence("TalA");
    }
    @Test
    public void getCtSequence_LAltA() {
        testCtSequence("LAltA");
    }
    @Test
    public void getCtSequence_GalA() {
        testCtSequence("GalA");
    }
    @Test
    public void getCtSequence_IdoA() {
        testCtSequence("IdoA");
    }
    @Test
    public void getCtSequence_DeoxyIdoA() {
        testCtSequence("DeoxyIdoA");
    }
    @Test
    public void getCtSequence_DeoxyGalA() {
        testCtSequence("DeoxyGalA");
    }
    @Test
    public void getCtSequence_DeoxyGlcA() {
        testCtSequence("DeoxyGlcA");
    }
    @Test
    public void getCtSequence_GroA2() {
        testCtSequence("GroA2");
    }
    @Test
    public void getCtSequence_LGulA() {
        testCtSequence("LGulA");
    }
    @Test
    public void getCtSequence_AllA() {
        testCtSequence("AllA");
    }



}