package org.expasy.sugarconverter.parser.composed;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Amino {

    Map<String, String> aminos = Test_Dictionnaries.getAminos();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all aminos");
        System.out.println(Test_Dictionnaries.getAminos().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, aminos);
    }


    @Test
    public void getCtSequence_TalN() {
        testCtSequence("TalN");
    }
    @Test
    public void getCtSequence_deoxyGlcN() {
        testCtSequence("deoxyGlcN");
    }
    @Test
    public void getCtSequence_ManN() {
        testCtSequence("ManN");
    }
    @Test
    public void getCtSequence_GlcN() {
        testCtSequence("GlcN");
    }
    @Test
    public void getCtSequence_HexN() {
        testCtSequence("HexN");
    }
    @Test
    public void getCtSequence_AllN() {
        testCtSequence("AllN");
    }
    @Test
    public void getCtSequence_GulN() {
        testCtSequence("GulN");
    }
    @Test
    public void getCtSequence_GlcNH2() {
        testCtSequence("GlcNH2");
    }
    @Test
    public void getCtSequence_LIdoN() {
        testCtSequence("LIdoN");
    }
    @Test
    public void getCtSequence_GlcNS() {
        testCtSequence("GlcNS");
    }
    @Test
    public void getCtSequence_deoxyGalN() {
        testCtSequence("deoxyGalN");
    }
    @Test
    public void getCtSequence_GalN() {
        testCtSequence("GalN");
    }
    @Test
    public void getCtSequence_LAltN() {
        testCtSequence("LAltN");
    }

}