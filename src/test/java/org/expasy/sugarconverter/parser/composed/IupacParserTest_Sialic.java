package org.expasy.sugarconverter.parser.composed;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Sialic {

    Map<String, String> sialics = Test_Dictionnaries.getSialics();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all sialic acids");
        System.out.println(Test_Dictionnaries.getSialics().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, sialics);
    }

    @Test
    public void getCtSequence_NeuGc() {
        testCtSequence("NeuGc");
    }
    @Test
    public void getCtSequence_Neu5_Ac2() {
        testCtSequence("Neu5,?Ac2");
    }
    @Test
    public void getCtSequence_Neu5Ac() {
        testCtSequence("Neu5Ac");
    }
    @Test
    public void getCtSequence_Neu_c() {
        testCtSequence("Neu?c");
    }
    @Test
    public void getCtSequence_NeuAc() {
        testCtSequence("NeuAc");
    }
    @Test
    public void getCtSequence_Neu45Ac2() {
        testCtSequence("Neu4,5Ac2");
    }
    @Test
    public void getCtSequence_NeuAc2() {
        testCtSequence("NeuAc2");
    }
    @Test
    public void getCtSequence_Neu59Ac2() {
        testCtSequence("Neu5,9Ac2");
    }
    @Test
    public void getCtSequence_Neu5__Ac3() {
        testCtSequence("Neu5,?,?Ac3");
    }



}