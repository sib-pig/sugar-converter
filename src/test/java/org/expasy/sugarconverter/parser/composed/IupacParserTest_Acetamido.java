package org.expasy.sugarconverter.parser.composed;

import org.expasy.sugarconverter.test.Test_Common;
import org.expasy.sugarconverter.test.Test_Dictionnaries;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_Acetamido {

    Map<String, String> acetamido = Test_Dictionnaries.getAcetamidos();


    @BeforeClass
    public static void setUp(){

        System.out.println("@Before - loading all acetamidos");
        System.out.println(Test_Dictionnaries.getAcetamidos().keySet());
    }

    public void testCtSequence(String iupac) {

        Test_Common.testCtSequenceDict(iupac, acetamido);
    }

    @Test
    public void getCtSequence_AllNAc() {
        testCtSequence("AllNAc");
    }
    @Test
    public void getCtSequence_GlcNAcol() {
        testCtSequence("GlcNAcol");
    }
    @Test
    public void getCtSequence_deoxyGlcNAc() {
        testCtSequence("deoxyGlcNAc");
    }
    @Test
    public void getCtSequence_LFucNAc() {
        testCtSequence("LFucNAc");
    }
    @Test
    public void getCtSequence_GlcNGc() {
        testCtSequence("GlcNGc");
    }
    @Test
    public void getCtSequence_deoxyGalNAc() {
        testCtSequence("deoxyGalNAc");
    }
    @Test
    public void getCtSequence_LIdoNAc() {
        testCtSequence("LIdoNAc");
    }
    @Test
    public void getCtSequence_BacAc2() {
        testCtSequence("BacAc2");
    }
    @Test
    public void getCtSequence_HexNAcol() {
        testCtSequence("HexNAcol");
    }
    @Test
    public void getCtSequence_FucNAc() {
        testCtSequence("FucNAc");
    }
    @Test
    public void getCtSequence_GalNAc() {
        testCtSequence("GalNAc");
    }
    @Test
    public void getCtSequence_TalNAc() {
        testCtSequence("TalNAc");
    }
    @Test
    public void getCtSequence_QuiNAc() {
        testCtSequence("QuiNAc");
    }
    @Test
    public void getCtSequence_HexNAc() {
        testCtSequence("HexNAc");
    }
    @Test
    public void getCtSequence_GulNAc() {
        testCtSequence("GulNAc");
    }
    @Test
    public void getCtSequence_DFucNAc() {
        testCtSequence("DFucNAc");
    }
    @Test
    public void getCtSequence_ManNAc() {
        testCtSequence("ManNAc");
    }
    @Test
    public void getCtSequence_GlcNAc() {
        testCtSequence("GlcNAc");
    }
    @Test
    public void getCtSequence_LRhaNAc() {
        testCtSequence("LRhaNAc");
    }
    @Test
    public void getCtSequence_GalNAcol() {
        testCtSequence("GalNAcol");
    }
    @Test
    public void getCtSequence_LAltNAc() {
        testCtSequence("LAltNAc");
    }


}