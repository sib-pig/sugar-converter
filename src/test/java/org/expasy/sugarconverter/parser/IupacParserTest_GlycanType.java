package org.expasy.sugarconverter.parser;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by julien on 07.03.17.
 */
public class IupacParserTest_GlycanType {

    /*N-LINKED (-> beta), O-LINKED (-> alpha) */
    public final String IUPAC = "Gal";

    public final String EXP_NLINKED = "RES\n" + "1b:b-dgal-HEX-1:5" ;
    public final String EXP_OLINKED = "RES\n" + "1b:a-dgal-HEX-1:5" ;
    public final String EXP_NOTLINKED = "RES\n" + "1b:x-dgal-HEX-1:5" ;




    public String getGlycoCtSequence(String iupac, String glycanType) {

        IupacParser p= new IupacParser(iupac);
        p.setGlycanType(glycanType);
        String output=null;

        try{
            p.getCtTree(p.parse());
            output = p.getCtSequence();
        }catch(Exception ex){
            System.err.println("Problem parsing the sequence" + ex.getMessage());
        }
        return output;
    }


    @Test
    public void getCtSequence_Nlinked() {

        String glycoct = getGlycoCtSequence(IUPAC,Constants.N_LINKED);

        assertNotNull(glycoct);
        assertFalse(glycoct.equals(""));
        assertTrue(glycoct.equals(EXP_NLINKED));
        assertFalse(glycoct.equals(EXP_OLINKED));
        assertFalse(glycoct.equals(EXP_NOTLINKED));
    }

    @Test
    public void getCtSequence_Olinked() {

        String glycoct = getGlycoCtSequence(IUPAC,Constants.O_LINKED);

        assertNotNull(glycoct);
        assertFalse(glycoct.equals(""));
        assertFalse(glycoct.equals(EXP_NLINKED));
        assertTrue(glycoct.equals(EXP_OLINKED));
        assertFalse(glycoct.equals(EXP_NOTLINKED));
    }

    @Test
    public void getCtSequence_Notlinked() {

        String glycoct = getGlycoCtSequence(IUPAC,Constants.NOT_LINKED);

        assertNotNull(glycoct);
        assertFalse(glycoct.equals(""));
        assertFalse(glycoct.equals(EXP_NLINKED));
        assertFalse(glycoct.equals(EXP_OLINKED));
        assertTrue(glycoct.equals(EXP_NOTLINKED));
    }

}